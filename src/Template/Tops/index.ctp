
  <!-- /top_info -->
  <div class="clearfix inner">
    <main class="left">
      <div class="no_login_ban">
        <a href="/c9fdo/users/login" class="btn_green">ログインして相談する</a>
        <p>会員登録がお済みでない方は<br><a href="/c9fdo/users/add">無料会員登録ページ</a>へ</p>
      </div>
      <!-- /no_login_ban -->
      <div class="login_ban">
        <a href="/c9fdo/users/add" class="btn_green"><i class="fa fa-angle-right" aria-hidden="true"></i> 今すぐプロに無料相談</a>
      </div>
      <!-- /login_ban -->

      <div class="keywords_search">
        <?php print_r($content)?>
        <h2>１２３キーワードで不動産に関するQ&amp;Aを探す</h2>
        <div>
          <form action="" method="get" class="clearfix">
            <input type="text" name="" placeholder="キーワードを入力して下さい。" class="left">
            <button type="" class="right"><i class="fa fa-search" aria-hidden="true"></i>検索する</button>
          </form>
          <p class="color_blue">例）ペット、楽器、境界紛争、敷金返還、バンド目指して上京、声優目指して上京、等。</p>
        </div>
      </div>
      <!-- /keywords_search -->
      <div class="category_search">
        <h2>カテゴリから不動産に関するQ&amp;Aを探す</h2>
        <div class="clearfix category_list">
          

          <?php foreach ($contents as $content):?>
          <div class="left">
          
            <p><a href="/c9fdo/articles/categorieindex/2"><?= $content->name ?></a></p>
            <div class="clearfix">
              <img src="img/top/<?= $content->icon ?>" alt="不動産売買" class="left">
              <ul class="left clearfix">
                <?php foreach ($content->middle_cotegories as $middle_cotegory):?>
                <li><a href="/c9fdo/articles/categorieindex/2/6"><?= $middle_cotegory->name ?></a></li>
                
                <?php endforeach; ?>
              </ul>
            </div>
          </div>
          <?php endforeach; ?>
        </div>
      </div>
      <!-- /category_search -->
      <div class="area_search">
        <h2>都道府県から専門家を探す</h2>
        <div>
          <p>不動産ドットコム」では<br>評判の良い不動産専門家＆<br>不動産業者を厳選しています。</p>
          <a href="">不動産ドットコムの取組</a>

          <a href="" class="hokkaido">北海道</a>
          <form action="" method="get">
            <div class="select_wrap tohoku">
              <select>
                <option value="">東北地方</option>
                <option value="青森">青森</option>
                <option value="岩手">岩手</option>
                <option value="宮城">宮城</option>
                <option value="秋田">秋田</option>
                <option value="山形">山形</option>
                <option value="福島">福島</option>
              </select>
            </div>

            <div class="select_wrap kanto">
              <select>
                <option value="">関東地方</option>
                <option value="茨城">茨城</option>
                <option value="栃木">栃木</option>
                <option value="群馬">群馬</option>
                <option value="埼玉">埼玉</option>
                <option value="千葉">千葉</option>
                <option value="東京">東京</option>
                <option value="神奈川">神奈川</option>
              </select>
            </div>

            <div class="select_wrap chubu_hukuriku">
              <select>
                <option value="">中部･北陸地方</option>
                <option value="新潟">新潟</option>
                <option value="富山">富山</option>
                <option value="石川">石川</option>
                <option value="福井">福井</option>
                <option value="山梨">山梨</option>
                <option value="長野">長野</option>
                <option value="岐阜">岐阜</option>
                <option value="静岡">静岡</option>
                <option value="愛知">愛知</option>
              
              </select>
            </div>

            <div class="select_wrap kinki">
              <select>
                <option value="">近畿地方</option>
                <option value="茨木">大阪</option>
                <option value="栃木">京都</option>
                <option value="群馬">兵庫</option>
                <option value="埼玉">奈良</option>
                <option value="千葉">三重</option>
                <option value="東京">滋賀</option>
                <option value="神奈川">和歌山</option>
              </select>
            </div>

            <div class="select_wrap chugoku_shikoku">
              <select>
                <option value="">中国･四国地方</option>
                <option value="鳥取">鳥取</option>
                <option value="島根">島根</option>
                <option value="岡山">岡山</option>
                <option value="広島">広島</option>
                <option value="山口">山口</option>
                <option value="徳島">徳島</option>
                <option value="香川">香川</option>
                <option value="愛媛">愛媛</option>
                <option value="高知">高知</option>
              </select>
            </div>

            <div class="select_wrap kyusyu_okinawa">
              <select>
                <option value="">九州･沖縄地方</option>
                <option value="福岡">福岡</option>
                <option value="佐賀">佐賀</option>
                <option value="長崎">長崎</option>
                <option value="熊本">熊本</option>
                <option value="大分">大分</option>
                <option value="宮崎">宮崎</option>
                <option value="鹿児島">鹿児島</option>
                <option value="沖縄">沖縄</option>
              </select>
            </div>

          </form>
        </div>
      </div>
      <!-- /area_search -->

      <div class="estimate_ban">
        <a href="" class="btn_green">複数の専門家に一括見積り</a>

      </div>
      <!-- /estimate_ban -->

      <div class="top_news">
        <h2>不動産ドットコムニュース</h2>
        <div class="clearfix">
          <div class="left">
            <a href=""><img src="img/top/dummy_news_thumb01.png" alt=""></a>
            <p><a href="">首都圏中古マンション価格引き続き上昇</a></p>
          </div>
          <div class="left">
            <a href=""><img src="img/top/dummy_news_thumb02.png" alt=""></a>
            <p><a href="">未婚女性の住宅購入理由、トップは「家賃を払い続けるのがも…</a></p>
          </div>
          <div class="left">
            <a href=""><img src="img/top/dummy_news_thumb03.png" alt=""></a>
            <p><a href="">三鷹駅直結タワーマンション販売相鉄不動産と三菱地所レジ…</a></p>
          </div>
          <div class="left">
            <a href=""><img src="img/top/dummy_news_thumb04.png" alt=""></a>
            <p><a href="">11月の首都圏マンション市場、新規発売戸数は3ヶ月ぶりに…</a></p>
          </div>
          <div class="left">
            <a href=""><img src="img/top/dummy_news_thumb05.png" alt=""></a>
            <p><a href="">11月の分譲マンション賃料、首都圏は3ヵ月連続上昇、東京…</a></p>
          </div>
          <div class="left">
            <a href=""><img src="img/top/dummy_news_thumb01.png" alt=""></a>
            <p><a href="">11月の近畿圏マンション市場、契約率は77.7%と引き続…</a></p>
          </div>
        </div>

        <div class="text-center">
          <a href="" class="btn_border">ニュースをもっと見る</a>
        </div>
        
      </div>
      <!-- /.top_news -->

      <div class="top_consultation">
        <h2>新着不動産相談</h2>
        <ul>
          <li><a href="">事務所＆店舗物件の敷金負担を減らす方法をおしえて？</a></li>
          <li><a href="">オール電化と、ガス併用住宅のメリットとデメリットについて教えてください</a></li>
          <li><a href="">銀行に新築費用のローンを申込んだところ、断られてしまいました。どうしたらよい…</a></li>
          <li><a href="">事務所＆店舗物件の敷金負担を減らす方法をおしえて？</a></li>
        </ul>
        <div class="text-center">
          <a href="" class="btn_border">相談をもっと見る</a>
        </div>
      </div>
      <!-- /.top_consultation-->
    </main>
