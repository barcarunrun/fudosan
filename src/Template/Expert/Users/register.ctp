<main class="inner">
        <ul class="bread">
            <li><a href="">不動産ドットコムTOP</a></li>
            <li>専門家登録</li>
        </ul>
        <article>
            <ol class="stepbar step4">
                <li class="step current">①登録情報の入力</li>
                <li class="step">②登録情報の入力2</li>
                <li class="step">③登録情報確認</li>
                <li class="step">④登録完了</li>
            </ol>
            <div class="error center">
                <p class="bold"><i class="fa fa-check-circle" aria-hidden="true"></i> 入力エラー</p>
                <ul>
                    <li>エラー詳細エラー詳細エラー詳細エラー詳細エラー詳細エラー詳細エラー詳細エラー詳細エラー詳細エラー詳細</li>
                    <li>エラー詳細エラー詳細エラー詳細エラー詳細エラー詳細エラー詳細エラー詳細エラー詳細エラー詳細エラー詳細エラー詳細</li>
                </ul>
            </div>
            <h2>登録情報の入力</h2>
            <div class="question_form_box">
                <div class="clearfix warning">
                    <p class="left"><i class="fa fa-check-circle" aria-hidden="true"></i>相談投稿における注意</p>
                    <p class="left">ご投稿後の変更・削除は原則行なっておりません。
                        <br>相談内容は一般に公開されるため、個人を特定されることの無いよう入力内容は十分ご注意ください。</p>
                </div>
                <form action="">
                    <table>
                        <tbody>
                            <tr>
                                <th>希望する<span class="must">必須</span></th>
                                <td class="q_tit">
                                    <input type="text">
                                    <p>半角のアルファベットまたは数字で、<span class="bold">4文字以上32文字以内で入力してください。</span>
                                        <br>IDは、登録完了後に変更できませんのでご注意ください。</p>
                                </td>
                            </tr>
                            <tr>
                                <th>都道府県<span class="must">必須</span></th>
                                <td class="q_tit">
                                    <div class="select_wrap">
                                        <select name="">
                                            <option value="" selected>地域を選択してください</option>
                                            <option value="北海道">北海道</option>
                                            <option value="青森県">青森県</option>
                                            <option value="岩手県">岩手県</option>
                                            <option value="宮城県">宮城県</option>
                                            <option value="秋田県">秋田県</option>
                                            <option value="山形県">山形県</option>
                                            <option value="福島県">福島県</option>
                                            <option value="茨城県">茨城県</option>
                                            <option value="栃木県">栃木県</option>
                                            <option value="群馬県">群馬県</option>
                                            <option value="埼玉県">埼玉県</option>
                                            <option value="千葉県">千葉県</option>
                                            <option value="東京都">東京都</option>
                                            <option value="神奈川県">神奈川県</option>
                                            <option value="新潟県">新潟県</option>
                                            <option value="富山県">富山県</option>
                                            <option value="石川県">石川県</option>
                                            <option value="福井県">福井県</option>
                                            <option value="山梨県">山梨県</option>
                                            <option value="長野県">長野県</option>
                                            <option value="岐阜県">岐阜県</option>
                                            <option value="静岡県">静岡県</option>
                                            <option value="愛知県">愛知県</option>
                                            <option value="三重県">三重県</option>
                                            <option value="滋賀県">滋賀県</option>
                                            <option value="京都府">京都府</option>
                                            <option value="大阪府">大阪府</option>
                                            <option value="兵庫県">兵庫県</option>
                                            <option value="奈良県">奈良県</option>
                                            <option value="和歌山県">和歌山県</option>
                                            <option value="鳥取県">鳥取県</option>
                                            <option value="島根県">島根県</option>
                                            <option value="岡山県">岡山県</option>
                                            <option value="広島県">広島県</option>
                                            <option value="山口県">山口県</option>
                                            <option value="徳島県">徳島県</option>
                                            <option value="香川県">香川県</option>
                                            <option value="愛媛県">愛媛県</option>
                                            <option value="高知県">高知県</option>
                                            <option value="福岡県">福岡県</option>
                                            <option value="佐賀県">佐賀県</option>
                                            <option value="長崎県">長崎県</option>
                                            <option value="熊本県">熊本県</option>
                                            <option value="大分県">大分県</option>
                                            <option value="宮崎県">宮崎県</option>
                                            <option value="鹿児島県">鹿児島県</option>
                                            <option value="沖縄県">沖縄県</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>添付ファイル</th>
                                <td>
                                    <label for="file" class="file">
                                        <img src="/c9fdo/img/under/form_file.png" alt="添付ファイルを選択">
                                        <input type="file" id="file" />
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <th>回答通知設定</th>
                                <td class="q_tit">
                                    <input type="radio" id="radio01" name="demo">
                                    <label for="radio01">回答がついたら通知をする</label>
                                    <input type="radio" id="radio02" name="demo">
                                    <label for="radio02">通知をしない</label>
                                    <p>※通知メールは会員登録のメールアドレスに送信されます。</p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <p class="text-center agree"><a href="">利用規約</a> 、<a href="">プライバシーポリシー</a> をよくお読みのうえ、「回答する」のボタンをクリックしてください。</p>
                    <button type="submit" class="btn_orange center">同意して確認画面に進む</button>
                </form>
            </div>
            <a class="modal_open" data-target="modal01">モーダルボタン</a>
            <div class="modal_content" id="modal01">
                <div class="center">
                    <p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
                    <div class="text-center">
                        <a class="modal_close">閉じる</a>
                    </div>
                </div>
            </div>
        </article>
    </main>