<div class="clearfix inner">
        <main class="left">
            <ul class="bread">
                <li><a href="/c9fdo/index">不動産ドットコムTOP</a></li>
                <li><a href="/c9fdo/questions/inventory_l">相談</a></li>
                <li>検索結果「あああ」</li>
            </ul>
            <section>
                <h2><i class="fa fa-search" aria-hidden="true"></i>「相続」の相談</h2>
                <p class="search_result_summary">「あああ」に関する不動産相談です。リフォーム分野に、「あああ」に関連する相談が多くよせられています。</p>
                <p class="search_hit_count bold">158件見つかりました。 11-20件目</p>
                <!--<?php// foreach ($ as $):?>-->
                <?php
                for($i=0;$i<10;$i+=1){echo'
                <article class="search_ar_box">
                    <p class="search_ar_tit bold"><a href="/c9fdo/questions/view">相談タイトル</a></p>
                    <div>
                        <ul>
                            <li>相続</li>
                            <li>遺産分割</li>
                            <li>相続財産</li>
                        </ul>
                        <p>相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章・・・</p>
                        <div class="clearfix">
                            <time class="left">2018年01月01日</time>
                            <p class="expert_answer_count left">専門家回答<span>4</span>件</p>
                        </div>
                    </div>
                </article>
                ';} ?>
                <!--<?php// endforeach; ?>-->
                
                <p class="search_hit_count bold">158件見つかりました。 11-20件目</p>
            </section>
            <div class="pager">
                <ul>
                    <li>
                        <a href="#">
                            <</a>
                    </li>
                    <li><a href="#">1</a></li>
                    <li class="current"><span>2</span></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><span>...</span></li>
                    <li><a href="#">16</a></li>
                    <li><a href="#">></a></li>
                    <li><a href="#">>></a></li>
                </ul>
            </div>
        </main>
