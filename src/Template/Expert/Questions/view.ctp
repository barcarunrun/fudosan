<div class="clearfix inner">
        <main class="left">
            <ul class="bread">
                <li><a href="/c9fdo/index">不動産ドットコムTOP</a></li>
                <li><a href="/c9fdo/questions/inventory_l/<?= $category_l . '">' . $current_category->parent->parent->name ?></a></li>
                <li><a href="/c9fdo/questions/inventory_m/<?= $category_l . '/' . $category_m . '">' . $current_category->parent->name ?></a></li>
                <li><a href="/c9fdo/questions/inventory_s/<?= $category_l . '/' . $category_m . '/' . $category_s . '">' . $current_category->name ?></a></li>
                <li><?= $result["title"] ?></li>
            </ul>
            
            <!--<?//= print_r($current_user) ?>-->
            <!--<?//= print_r($result) ?>-->
            <section class="question">
                <div class="question_box">
                    <h1><span>Q.</span><?= $result["title"] ?></h1>
                    <p><?= $result["body"] ?></p>
                    <ul class="attachment">
                        <li><a href=""><img src="/c9fdo/img/under/attachment_dummy.png" alt="" class="img_responsive"></a></li>
                        <li><a href=""><img src="/c9fdo/img/under/attachment_dummy.png" alt="" class="img_responsive"></a></li>
                    </ul>
                    <div class="clearfix">
                        <time class="left"><?php $created_date = new DateTime($result["created"]);echo $created_date->format('Y年m月d日'); ?>
                        <span><?php if(isset($result["modified"])){$created_date = new DateTime($result["modified"]);echo $created_date->format('(追記: Y年m月d日)');} ?></span></time>
                        <p class="account_name right bold"><a href="">saitouhana</a>さん</p>
                    </div>
                </div>
                <div class="text-center">
                    <!-- ログインで表示変更 -->
                    <?php
                    if(isset($current_user)){
                        if($current_user["id"] == $result["user_id"]){
                            echo '<a href="" class="btn_orange">質問に追記する</a>';
                        }
                        if(isset($current_user["expert"])){
                            echo '<a href="" class="btn_orange">質問に回答する</a>';
                        }
                    }
                    ?>
                </div>
            </section>
            <div class="clearfix related_consult">
                <p class="bold"><span>ここもチェック！</span>この質問への関連度の高い相談</p>
                <div class="left">
                    <p class="related_consult_tit bold">
                        質問タイトル質問タイトル質問タイトル質問タイトル質問タイトル
                    </p>
                    <p>本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文・・・</p>
                </div>
                <div class="left">
                    <p class="related_consult_tit bold">
                        質問タイトル質問タイトル質問タイトル質問タイトル質問タイトル
                    </p>
                    <p>本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文本文・・・</p>
                </div>
            </div>
            <div class="expert_answer">
                <h2><i class="fa fa-home"></i> 専門家の回答</h2>
                <div class="clearfix expert_answer_info">
                    <img src="/c9fdo/img/under/expert_answer_thumb.png" alt="" class="left">
                    <div class="left">
                        <p class="bold">山本太郎 <span>弁護士</span></p>
                        <p><i class="fa fa-map-marker"></i> 東京 > 千代田区</p>
                    </div>
                    <div class="clearfix right">
                        <button class="left thanks"><i class="fa fa-heart-o"></i> ありがとう</button>
                        <button class="left best"><i class="fa fa-thumbs-up"></i> ベストアンサー</button>
                    </div>
                </div>
                <p>回答内容回答内容回答内容回答内容回答内容回答内容回答内容回答内容回答内容回答内容回答内容回答内容回答内容回答内容回答内容回答内容回答内容回答内容回答内容回答内容回答内容回答内容回答内容回答内容回答内容回答内容回答内容回答内容回答内容回答内容回答内容回答内容</p>
                <div class="text-right">
                    <time>2018年6月20日</time>
                </div>
                <div class="clearfix big_conv_button">
                    <div class="left">
                        <button class="left thanks"><i class="fa fa-heart-o"></i> ありがとう</button>
                        <p class="left">各回答ごとに押せます
                            <br>※<span class="bold">一度押したら取り消せません</span></p>
                    </div>
                    <div class="left">
                        <button class="left best"><i class="fa fa-thumbs-up"></i> ベストアンサー</button>
                        <p class="left">1つの回答のみ押せます
                            <br>※<span class="bold">一度押したら取り消せません</span></p>
                    </div>
                </div>
                <div class="owner_answer">
                    <p class="account_name bold">saitouhanaさん(質問者)</p>
                    <p>返信内容返信内容返信内容返信内容返信内容返信内容返信内容返信内容返信内容返信内容返信内容返信内容返信内容返信内容返信内容返信内容返信内容返信内容返信内容返信内容返信内容返信内容返信内容返信内容返信内容返信内容返信内容返信内容返信内容返信内容返信内容返信内容返信内容返信内容返信内容返信内容</p>
                    <div class="text-right">
                        <time>2018年6月20日</time>
                    </div>
                </div>
                <div class="text-center">
                    <a href="" class="btn_orange">返信を追加する</a>
                </div>
            </div>
            <div class="keywords_search">
                <h2>条件を変えて検索する</h2>
                <div>
                    <?= $this->Form->create($keywords,['class'=>'clearfix']) ?>
                    <?= $this->Form->control('title',['type'=>'text','placeholder'=>'キーワードを入力して下さい。','class'=>'left']) ?>
                    <?= '<button type="submit" class="right"><i class="fa fa-search" aria-hidden="true"></i>検索する</button>' ?>
                    <!--<?//= $this->Form->button('ログイン',['class'=>'btn_green small width02']) ?>-->
                    <?= $this->Form->end() ?>
                    <!--<form action="" method="get" class="clearfix">-->
                    <!--    <input type="text" name="" placeholder="キーワードを入力して下さい。" class="left">-->
                    <!--    <button type="submit" class="right"><i class="fa fa-search" aria-hidden="true"></i>検索する</button>-->
                    <!--</form>-->
                    <p class="color_blue">例）ペット、楽器、境界紛争、敷金返還、バンド目指して上京、声優目指して上京、等。</p>
                </div>
            </div>
            <!-- /keywords_search -->
            <div class="login_ban pc">
                <a href="" class="btn_green"><i class="fa fa-angle-right" aria-hidden="true"></i> 今すぐプロに無料相談</a>
            </div>
            <!-- /login_ban -->
            <div class="estimate_ban pc">
                <a href="" class="btn_green">複数の専門家に一括見積り</a>
            </div>
            <!-- /estimate_ban -->
            <div class="sp_estimate_ban sp">
                <a href=""><img src="/c9fdo/img/top/sp_estimate_ban.png" alt="たった1回の入力で、複数の専門家からお見積可能です" class="img_responsive"></a>
            </div>
            <!-- /estimate_ban -->
            <div class="area_search">
                <h2>都道府県から専門家を探す</h2>
                <div class="pc">
                    <p>不動産ドットコム」では
                        <br>評判の良い不動産専門家＆
                        <br>不動産業者を厳選しています。</p>
                    <a href="">不動産ドットコムの取組</a>
                    <a href="" class="hokkaido">北海道</a>
                    <form action="" method="get">
                        <div class="select_wrap tohoku">
                            <select>
                                <option value="">東北地方</option>
                                <option value="青森">青森</option>
                                <option value="岩手">岩手</option>
                                <option value="宮城">宮城</option>
                                <option value="秋田">秋田</option>
                                <option value="山形">山形</option>
                                <option value="福島">福島</option>
                            </select>
                        </div>
                        <div class="select_wrap kanto">
                            <select>
                                <option value="">関東地方</option>
                                <option value="茨城">茨城</option>
                                <option value="栃木">栃木</option>
                                <option value="群馬">群馬</option>
                                <option value="埼玉">埼玉</option>
                                <option value="千葉">千葉</option>
                                <option value="東京">東京</option>
                                <option value="神奈川">神奈川</option>
                            </select>
                        </div>
                        <div class="select_wrap chubu_hukuriku">
                            <select>
                                <option value="">中部･北陸地方</option>
                                <option value="新潟">新潟</option>
                                <option value="富山">富山</option>
                                <option value="石川">石川</option>
                                <option value="福井">福井</option>
                                <option value="山梨">山梨</option>
                                <option value="長野">長野</option>
                                <option value="岐阜">岐阜</option>
                                <option value="静岡">静岡</option>
                                <option value="愛知">愛知</option>
                            </select>
                        </div>
                        <div class="select_wrap kinki">
                            <select>
                                <option value="">近畿地方</option>
                                <option value="茨木">大阪</option>
                                <option value="栃木">京都</option>
                                <option value="群馬">兵庫</option>
                                <option value="埼玉">奈良</option>
                                <option value="千葉">三重</option>
                                <option value="東京">滋賀</option>
                                <option value="神奈川">和歌山</option>
                            </select>
                        </div>
                        <div class="select_wrap chugoku_shikoku">
                            <select>
                                <option value="">中国･四国地方</option>
                                <option value="鳥取">鳥取</option>
                                <option value="島根">島根</option>
                                <option value="岡山">岡山</option>
                                <option value="広島">広島</option>
                                <option value="山口">山口</option>
                                <option value="徳島">徳島</option>
                                <option value="香川">香川</option>
                                <option value="愛媛">愛媛</option>
                                <option value="高知">高知</option>
                            </select>
                        </div>
                        <div class="select_wrap kyusyu_okinawa">
                            <select>
                                <option value="">九州･沖縄地方</option>
                                <option value="福岡">福岡</option>
                                <option value="佐賀">佐賀</option>
                                <option value="長崎">長崎</option>
                                <option value="熊本">熊本</option>
                                <option value="大分">大分</option>
                                <option value="宮崎">宮崎</option>
                                <option value="鹿児島">鹿児島</option>
                                <option value="沖縄">沖縄</option>
                            </select>
                        </div>
                    </form>
                </div>
                <div class="sp">
                    <form action="" method="get">
                        <div class="select_wrap">
                            <select name="">
                                <option value="" selected>都道府県</option>
                                <option value="北海道">北海道</option>
                                <option value="青森県">青森県</option>
                                <option value="岩手県">岩手県</option>
                                <option value="宮城県">宮城県</option>
                                <option value="秋田県">秋田県</option>
                                <option value="山形県">山形県</option>
                                <option value="福島県">福島県</option>
                                <option value="茨城県">茨城県</option>
                                <option value="栃木県">栃木県</option>
                                <option value="群馬県">群馬県</option>
                                <option value="埼玉県">埼玉県</option>
                                <option value="千葉県">千葉県</option>
                                <option value="東京都">東京都</option>
                                <option value="神奈川県">神奈川県</option>
                                <option value="新潟県">新潟県</option>
                                <option value="富山県">富山県</option>
                                <option value="石川県">石川県</option>
                                <option value="福井県">福井県</option>
                                <option value="山梨県">山梨県</option>
                                <option value="長野県">長野県</option>
                                <option value="岐阜県">岐阜県</option>
                                <option value="静岡県">静岡県</option>
                                <option value="愛知県">愛知県</option>
                                <option value="三重県">三重県</option>
                                <option value="滋賀県">滋賀県</option>
                                <option value="京都府">京都府</option>
                                <option value="大阪府">大阪府</option>
                                <option value="兵庫県">兵庫県</option>
                                <option value="奈良県">奈良県</option>
                                <option value="和歌山県">和歌山県</option>
                                <option value="鳥取県">鳥取県</option>
                                <option value="島根県">島根県</option>
                                <option value="岡山県">岡山県</option>
                                <option value="広島県">広島県</option>
                                <option value="山口県">山口県</option>
                                <option value="徳島県">徳島県</option>
                                <option value="香川県">香川県</option>
                                <option value="愛媛県">愛媛県</option>
                                <option value="高知県">高知県</option>
                                <option value="福岡県">福岡県</option>
                                <option value="佐賀県">佐賀県</option>
                                <option value="長崎県">長崎県</option>
                                <option value="熊本県">熊本県</option>
                                <option value="大分県">大分県</option>
                                <option value="宮崎県">宮崎県</option>
                                <option value="鹿児島県">鹿児島県</option>
                                <option value="沖縄県">沖縄県</option>
                            </select>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /area_search -->
        </main>
