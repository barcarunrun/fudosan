<?php
//var_dump($_POST);
$page_flag = 0;
if( !empty($_POST['confirm']) ) {
	$page_flag = 1;
}elseif (!empty($_POST['submit'])) {
    $page_flag = 2; //完了画面
} else {
    $page_flag = 0; //入力画面
}
?>

<main class="inner">
        <ul class="bread">
            <li><a href="">不動産ドットコムTOP</a></li>
            <li><a href="">不動産相続</a></li>
            <li>相続の投稿</li>
        </ul>
        <article>
            <div class="estimate_ban02 pc">
                <a href="" class="btn_white bold">一括見積について詳しく見る</a>
            </div>
            <!-- /estimate_ban -->
            <div class="sp_estimate_ban02 sp">
                <a href=""><img src="/c9fdo/img/under/sp_estimate_ban02.png" alt="簡単入力で完全無料！複数の不動産の専門家に一括見積請求" class="img_responsive"></a>
            </div>
            <!-- /estimate_ban -->
            <p class="copy bold"><span>みんなの不動産相談</span>｜無料で優良不動産業者・専門家が回答する日本最大級の「不動産に関するQ＆Aサイト」</p>
            
            <?php if( $page_flag === 1 ): ?>
            
            <ol class="stepbar step3">
                <li class="step">①相談内容の入力</li>
                <li class="step current">②カテゴリの選択・相談内容の確認</li>
                <li class="step">③相談完了</li>
            </ol>
            
            <form method="post" accept-charset="utf-8" action="/c9fdo/questions/add">
                <h2>相談カテゴリの選択</h2>
                <div class="element_wrap">
            		<label>大カテゴリ</label>
            		<p><?= $this->Form->select('large_category_id',$large_categories,['class'=>'name']); ?></p>
            	</div>
            	<div class="element_wrap">
            		<label>中カテゴリ</label>
            		<!--<p><?//= $this->Form->select('middle_category_id',$middle_categories,['class'=>'name']); ?></p>-->
            	</div>
            	<div class="element_wrap">
            		<label>小カテゴリ</label>
            		<!--<p><?//= $this->Form->select('small_category_id',$small_categories,['class'=>'name']); ?></p>-->
            	</div>
                <h2>入力した相談内容の確認</h2>
            	<div class="element_wrap">
            		<label>タイトル</label>
            		<p><?= $_POST['title']; ?></p>
            	</div>
            	<div class="element_wrap">
            		<label>相談内容</label>
            		<p><?= $_POST['body']; ?></p>
            	</div>
            	<div class="element_wrap">
            	    <?php if($_POST['reply_notice'] !== 0){
            	        echo "<p>回答がついても通知をしない</p>";
            	    }else{
            	        echo "<p>回答がついたら通知をする</p>";
            	    } ?>
                </div>
            	<input type="submit" name="back" value="戻る">
            	<input type="submit" name="submit" value="送信">
            	<input type="hidden" name="title" value="<?= $_POST['title']; ?>">
            	<input type="hidden" name="body" value="<?= $_POST['body']; ?>">
            	<input type="hidden" name="user_id" value="<?= $_POST['user_id']; ?>">
            	<input type="hidden" name="created" value="<?= $_POST['created']; ?>">
            	<input type="hidden" name="reply_notice" value="<?= $_POST['reply_notice']; ?>">
            </form>
            
            <?php elseif ($page_flag === 2) : ?>
                <ol class="stepbar step3">
                    <li class="step">①相談内容の入力</li>
                    <li class="step">②カテゴリの選択・相談内容の確認</li>
                    <li class="step current">③相談完了</li>
                </ol>
                <p>送信完了しました！</p>
                <input class="" type="button" onClick="location.href='/c9fdo/users/mypage'" value="マイページに戻る">
            <?php else: ?>
            
            <ol class="stepbar step3">
                <li class="step current">①相談内容の入力</li>
                <li class="step">②カテゴリの選択・相談内容の確認</li>
                <li class="step">③相談完了</li>
            </ol>
            <h2>相談内容の入力</h2>
            <div class="question_form_box">
                <div class="clearfix warning">
                    <p class="left"><i class="fa fa-check-circle" aria-hidden="true"></i>相談投稿における注意</p>
                    <p class="left">ご投稿後の変更・削除は原則行なっておりません。
                        <br>相談内容は一般に公開されるため、個人を特定されることの無いよう入力内容は十分ご注意ください。</p>
                </div>
                <form method="post" accept-charset="utf-8" action="/c9fdo/questions/add">
                    <input type="hidden" name="user_id" value="<?= $current_user["id"] ?>">
                    <input type="hidden" name="created" value="<?= date("Y/m/d H:i:s") ?>">
                    <table>
                        <tbody>
                            <tr>
                                <th>タイトル<span class="must">必須</span></th>
                                <td class="q_tit">
                                    <input type="text" name="title" required="required" value="<?php if( !empty($_POST['title']) ){ echo $_POST['title']; } ?>">
                                    <p>入力できる文字数 : 残り <span class="bold">50</span> 字(10文字以上入力してください)</p>
                                    <div class="sample">
                                        <p class="sample_tit bold">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>回答がつきやすいタイトル
                                        </p>
                                        <p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
                                        <div class="example">
                                            <p class="example_tit"><i class="fa fa-thumbs-up"></i>良いタイトルの例</p>
                                            <p>テキストテキストテキストテキストテキストテキストテキスト</p>
                                            <p class="example_tit"><i class="fa fa-thumbs-down"></i>悪いタイトルの例</p>
                                            <p>テキストテキストテキストテキストテキストテキストテキスト</p>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>相談内容<span class="must">必須</span></th>
                                <td class="q_tit">
                                    <textarea name="body" required="required" value="<?php if( !empty($_POST['body']) ){ echo $_POST['body']; } ?>"></textarea>
                                    <p>入力できる文字数 : 残り <span class="bold">50</span> 字(10文字以上入力してください)</p>
                                    <div class="sample">
                                        <p class="sample_tit bold">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>回答がつきやすい投稿
                                        </p>
                                        <p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
                                        <div class="example">
                                            <p class="example_tit"><i class="fa fa-thumbs-up"></i>良い投稿の例</p>
                                            <p>テキストテキストテキストテキストテキストテキストテキスト</p>
                                            <p class="example_tit"><i class="fa fa-thumbs-down"></i>悪い投稿の例</p>
                                            <p>テキストテキストテキストテキストテキストテキストテキスト</p>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>添付ファイル</th>
                                <td>
                                    <label for="file" class="file">
                                        <img src="/c9fdo/img/under/form_file.png" alt="添付ファイルを選択">
                                        <input type="file" id="file" />
                                    </label>
                                </td>
                            </tr>
                            <tr>
                                <th>回答通知設定</th>
                                <td class="q_tit">
                                    <?php
                                    if( !empty($_POST['reply_notice']) && $_POST['reply_notice'] === 0){
                                            echo'
                                            <input type="radio" id="radio01" name="reply_notice" value="ture">
                                            <label for="radio01">回答がついたら通知をする</label>
                                            <input type="radio" id="radio02" name="reply_notice" value="0" checked="checked">
                                            <label for="radio02">通知をしない</label>
                                            ';
                                    }else{
                                        echo'
                                        <input type="radio" id="radio01" name="reply_notice" value="ture" checked="checked">
                                        <label for="radio01">回答がついたら通知をする</label>
                                        <input type="radio" id="radio02" name="reply_notice" value="0">
                                        <label for="radio02">通知をしない</label>
                                        ';    
                                    } ?>
                                    
                                    <p>※通知メールは会員登録のメールアドレスに送信されます。</p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <p class="text-center agree"><a href="">利用規約</a> 、<a href="">プライバシーポリシー</a> をよくお読みのうえ、「回答する」のボタンをクリックしてください。</p>
                    <button type="submit" class="btn_orange center" name="confirm" value="同意して確認画面に進む">同意して確認画面に進む</button>
                </form>
                <?php endif; ?>
            </div>
        </article>
    </main>