<?php
/**
  * @var \App\View\AppView $this
  */
?>
<div class="clearfix inner bordertop">
<main class="left">

<div class="articles index large-9 medium-8 columns content">
    
    <ul class="breadcrumb">
      <li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb">
        <a href="/c9fdo/tops" itemprop="url">
          <span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i>不動産ドットコムTOP</span>
        </a>
      </li>
      <li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb">
        <a href="" itemprop="url">
          <span itemprop="title"><?= $large_categorie_list[$large_num] ?></span>
        </a>
      </li>
    </ul>
    
    
    <h2><i class="fa fa-search" aria-hidden="true"></i>「<?= $large_categorie_list[$large_num] ?>」の相談</h2>
    <p class="p01">「<?= $large_categorie_list[$large_num] ?>」に関する不動産相談です。</p>
    <p class="p02 bold"><?= $this->Paginator->counter(
    ['format' => __('{{count}} 件みつかりました {{start}} - {{end}} 件目')]) ?></p>
    
    
    <?//= print_r(count($articles)) ?>
    <?//= print_r(gettype($articles)) ?>
    <ul>
      <?php foreach ($articles as $article): ?>
      <li class="articles_list">
          <!--<td><?//= $this->Number->format($article->id) ?></td>-->
          <!--<td><?//= $article->has('user') ? $this->Html->link($article->user->id, ['controller' => 'Users', 'action' => 'view', $article->user->id]) : '' ?></td>-->
          <div>
            <h3>
              <p class="i_question"><span>Q</span></p>
              <!--<a class="" href=""><?//= h($article->title) ?></a>-->
              <?= $this->Html->link($article->title, ['action' => 'view', $article->id]) ?>
            </h3>
          </div>
          <div class="paragraph bread">
            <?php $i=$article->mid_categorie_id - 1;$k=$mid_large_list[$i] - 1;
            echo $this->Html->link($large_categorie_list[$k], ['controller' => 'Articles', 'action' => 'categorieindex', $mid_large_list[$i]]) ?>
            &gt;
            <?php $e=$article->mid_categorie_id - 1;
            echo $this->Html->link($mid_categorie_list[$e], ['controller' => 'Articles', 'action' => 'categorieindex', "$mid_large_list[$i]","$article->mid_categorie_id"]) ?>
          </div>
          <p class="paragraph text"><?= h($article->body) ?></p>
          
          <p class="paragraph"><?= h($article->created) ?></p>
          <!--<td><?//= h($article->modified) ?></td>-->
          <!--<td class="actions">-->
          <!--    <?//= $this->Html->link(__('View'), ['action' => 'view', $article->id]) ?>-->
          <!--    <?//= $this->Form->postLink(__('Delete'), ['action' => 'delete', $article->id], ['confirm' => __('Are you sure you want to delete # {0}?', $article->id)]) ?>-->
          <!--</td>-->
      </li>
      <?php endforeach; ?>
        <!--</tbody>-->
    </ul>
    <p><?= $this->Paginator->counter(
    ['format' => __('{{count}} 件みつかりました {{start}} - {{end}} 件目')]) ?></p>
    <div class="paginator">
      <?= $this->Paginator->first('<<'); ?>
      <?= $this->Paginator->prev('<'); ?>
      <?= $this->Paginator->numbers(); ?>
      <?= $this->Paginator->next('>'); ?>
      <?= $this->Paginator->last('>>'); ?>
    </div>
</div>
</main>
<!--<main class="left">-->