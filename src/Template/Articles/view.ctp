<?php
/**
  * @var \App\View\AppView $this
  */
?>
<!--<nav class="large-3 medium-4 columns" id="actions-sidebar">-->
<!--    <ul class="side-nav">-->
<!--        <li class="heading"><?= __('Actions') ?></li>-->
<!--        <li><?= $this->Html->link(__('Edit Article'), ['action' => 'edit', $article->id]) ?> </li>-->
<!--        <li><?= $this->Form->postLink(__('Delete Article'), ['action' => 'delete', $article->id], ['confirm' => __('Are you sure you want to delete # {0}?', $article->id)]) ?> </li>-->
<!--        <li><?= $this->Html->link(__('List Articles'), ['action' => 'index']) ?> </li>-->
<!--        <li><?= $this->Html->link(__('New Article'), ['action' => 'add']) ?> </li>-->
<!--        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>-->
<!--        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>-->
<!--        <li><?= $this->Html->link(__('List Mid Categories'), ['controller' => 'MidCategories', 'action' => 'index']) ?> </li>-->
<!--        <li><?= $this->Html->link(__('New Mid Category'), ['controller' => 'MidCategories', 'action' => 'add']) ?> </li>-->
<!--    </ul>-->
<!--</nav>-->
<div class="articles view large-9 medium-8 columns content">
    
    <ul class="breadcrumb">
      <li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb">
        <a href="/c9fdo/tops" itemprop="url">
          <span itemprop="title"><i class="fa fa-home" aria-hidden="true"></i>不動産ドットコムTOP</span>
        </a>
      </li>
      <li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb">
        <a href="大カテゴリーＵＲＬ" itemprop="url">
          <span itemprop="title"><?= $large_categorie ?></span>
        </a>
      </li>
      <li itemscope="itemscope" itemtype="http://data-vocabulary.org/Breadcrumb">
        <a href="カテゴリーＵＲＬ" itemprop="url">
          <span itemprop="title"><?= $large_categorie ?></span>
        </a>
      </li>
    </ul>
    
    
    
    <h3><?= h($article->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $article->has('user') ? $this->Html->link($article->user->id, ['controller' => 'Users', 'action' => 'view', $article->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Title') ?></th>
            <td><?= h($article->title) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Slug') ?></th>
            <td><?= h($article->slug) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Mid Category') ?></th>
            <td><?= $article->has('mid_category') ? $this->Html->link($article->mid_category->name, ['controller' => 'MidCategories', 'action' => 'view', $article->mid_category->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($article->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($article->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($article->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Published') ?></th>
            <td><?= $article->published ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Body') ?></h4>
        <?= $this->Text->autoParagraph(h($article->body)); ?>
    </div>
</div>
