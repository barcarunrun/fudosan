<?php
// debug($prefectures);
?>
<main>
        <div class="center">
            <h1 class="text-center">不動産の専門家に相談するには<br><span>会員登録が必要です</span></h1>
            <ul class="clearfix sns_register">
                <li class="left"><a href="/c9fdo/questions/under_construction"><img src="/c9fdo/img/under/fb_register.png" alt="Facebookでログイン"></a></li>
                <li class="right"><a href="/c9fdo/questions/under_construction"><img src="/c9fdo/img/under/yahoo_register.png" alt="Yahoo!IDでログイン"></a></li>
                <li class="left"><a href="/c9fdo/questions/under_construction"><img src="/c9fdo/img/under/twitter_register.png" alt="Twitterでログイン"></a></li>
                <li class="right"><a href="/c9fdo/questions/under_construction"><img src="/c9fdo/img/under/line_register.png" alt="LINEでログイン"></a></li>
            </ul>
            <p>もしくは</p>
            <form action="/c9fdo/users/register" method="post" class="origin_register">
                <input type="hidden" name="nickname" value="user<?= mt_rand(0,9) . mt_rand(0,9) . mt_rand(0,9) . mt_rand(0,9); ?>">
                <input type="hidden" name="created" value="<?= date("Y/m/d H:i:s") ?>">
                <table>
                    <tbody>
                        <tr>
                            <th>メールアドレス</th>
                            <td>
                                <input type="email" name="email" placeholder="@を含むメールアドレスを入力">
                            </td>
                        </tr>
                        <tr>
                            <th>パスワード</th>
                            <td>
                                <input type="password" name="password" placeholder="半角英数字6~32文字のパスワード">
                            </td>
                        </tr>
                        <tr>
                            <th>パスワード確認用
                            </th>
                            <td>
                                <input type="password" name="" placeholder="パスワードを再入力して下さい">
                            </td>
                        </tr>
                        <tr>
                            <th>性別</th>
                            <td>
                                <label>
                                    <input type="radio" name="sex" value="male">男性</label>
                                <label>
                                    <input type="radio" name="sex" value="female">女性</label>
                            </td>
                        </tr>
                        
                        <tr>
                            <th>都道府県</th>
                            <td>
                                <div class="select_wrap">
                                    <select name="prefecture_id">
                                        <option value="" selected>選択して下さい</option>
                                        <?php foreach($prefectures as $prefecture):?>
                                        <?= '<option value="' . $prefecture["id"] . '">' . $prefecture["name"] . '</option>'; ?>
                                        <?php endforeach;?>
                                        <!--<option value="青森県">青森県</option>-->
                                        <!--<option value="岩手県">岩手県</option>-->
                                        <!--<option value="宮城県">宮城県</option>-->
                                        <!--<option value="秋田県">秋田県</option>-->
                                        <!--<option value="山形県">山形県</option>-->
                                        <!--<option value="福島県">福島県</option>-->
                                        <!--<option value="茨城県">茨城県</option>-->
                                        <!--<option value="栃木県">栃木県</option>-->
                                        <!--<option value="群馬県">群馬県</option>-->
                                        <!--<option value="埼玉県">埼玉県</option>-->
                                        <!--<option value="千葉県">千葉県</option>-->
                                        <!--<option value="東京都">東京都</option>-->
                                        <!--<option value="神奈川県">神奈川県</option>-->
                                        <!--<option value="新潟県">新潟県</option>-->
                                        <!--<option value="富山県">富山県</option>-->
                                        <!--<option value="石川県">石川県</option>-->
                                        <!--<option value="福井県">福井県</option>-->
                                        <!--<option value="山梨県">山梨県</option>-->
                                        <!--<option value="長野県">長野県</option>-->
                                        <!--<option value="岐阜県">岐阜県</option>-->
                                        <!--<option value="静岡県">静岡県</option>-->
                                        <!--<option value="愛知県">愛知県</option>-->
                                        <!--<option value="三重県">三重県</option>-->
                                        <!--<option value="滋賀県">滋賀県</option>-->
                                        <!--<option value="京都府">京都府</option>-->
                                        <!--<option value="大阪府">大阪府</option>-->
                                        <!--<option value="兵庫県">兵庫県</option>-->
                                        <!--<option value="奈良県">奈良県</option>-->
                                        <!--<option value="和歌山県">和歌山県</option>-->
                                        <!--<option value="鳥取県">鳥取県</option>-->
                                        <!--<option value="島根県">島根県</option>-->
                                        <!--<option value="岡山県">岡山県</option>-->
                                        <!--<option value="広島県">広島県</option>-->
                                        <!--<option value="山口県">山口県</option>-->
                                        <!--<option value="徳島県">徳島県</option>-->
                                        <!--<option value="香川県">香川県</option>-->
                                        <!--<option value="愛媛県">愛媛県</option>-->
                                        <!--<option value="高知県">高知県</option>-->
                                        <!--<option value="福岡県">福岡県</option>-->
                                        <!--<option value="佐賀県">佐賀県</option>-->
                                        <!--<option value="長崎県">長崎県</option>-->
                                        <!--<option value="熊本県">熊本県</option>-->
                                        <!--<option value="大分県">大分県</option>-->
                                        <!--<option value="宮崎県">宮崎県</option>-->
                                        <!--<option value="鹿児島県">鹿児島県</option>-->
                                        <!--<option value="沖縄県">沖縄県</option>-->
                                    </select>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <p class="text-center">「 <a href="/c9fdo/questions/under_construction">利用規約</a> 、<a href="/c9fdo/questions/under_construction">プライバシーポリシー</a> 」をお読みのうえ、
                    <br>下のボタンをクリックしてください。</p>
                <input type="submit" name="submit" value="上記の規約に同意して確認画面へ" class="center">
            </form>
        </div>
    </main>