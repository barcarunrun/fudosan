<!--<?php// var_dump($_POST) ?><br>-->
<!--<?php// debug($returnpage) ?>-->
<main>
        <div class="center">
            <?php if(is_null($flag_expert)){
                echo '<h1 class="text-center">不動産ドットコムにログインする</h1>';
            }else{
                echo '<h1 class="text-center">専門家としてログインする</h1>';
            }?>
            
            <div class="clearfix">
                <div class="left origin_login">
                    <?= $this->Form->create() ?>
                    <?= $this->Form->control('email',['type'=>'email','placeholder'=>'メールアドレス']) ?>
                    <?= $this->Form->control('password',['type'=>'password','placeholder'=>'パスワード']) ?>
                    <?= $this->Form->control('ログイン',['type'=>'submit','class'=>'center','value'=>'ログイン']) ?>
                    <!--<input type="submit" class="center" name="ログイン" value="ログイン" onclick="history.back();">-->
                    <?= $this->Form->end() ?>
                    
                    <ul>
                        <li><a href="/c9fdo/questions/under_construction">> メールアドレスをお忘れの方</a></li>
                        <li><a href="/c9fdo/questions/under_construction">> パスワードをお忘れの方</a></li>
                        <li><a href="/c9fdo/questions/under_construction">> 不動産ドットコムIDをお忘れの方</a></li>
                    </ul>
                </div>
                <div class="right sns_login">
                    <ul>
                        <li><a href="/c9fdo/questions/under_construction"><img src="/c9fdo/img/under/fb_login.png" alt="Facebookでログイン"></a></li>
                        <li><a href="/c9fdo/questions/under_construction"><img src="/c9fdo/img/under/yahoo_login.png" alt="Yahoo!IDでログイン"></a></li>
                        <li><a href="/c9fdo/questions/under_construction"><img src="/c9fdo/img/under/twitter_login.png" alt="Twitterでログイン"></a></li>
                        <li><a href="/c9fdo/questions/under_construction"><img src="/c9fdo/img/under/line_login.png" alt="LINEでログイン"></a></li>
                    </ul>
                </div>
            </div>
            <!--<a href="<?//= ['controller' => 'experts','action' => 'login']?>" class="expert_link text-center">不動産業者・専門家の方はこちら</a>-->
            <?php if(is_null($flag_expert)){
                echo '<a href="/c9fdo/expert/users/login" class="expert_link text-center">不動産業者・専門家の方はこちら</a>';
            }?>
            <!--<a href="/c9fdo/questions/under_construction" class="expert_link text-center">不動産業者・専門家の方はこちら</a>-->
        </div>
        
    </main>