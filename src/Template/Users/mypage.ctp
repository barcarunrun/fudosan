<div class="clearfix inner">
        <main class="left">
            <ul class="bread">
                <li><a href="/c9fdo/index">不動産ドットコムTOP</a></li>
                <li>マイページ</li>
            </ul>

            <h1>プロフィール情報の変更</h1>

            <form action="" method="get" class="mypage_register">
                <table>
                    <tbody>
                        <tr>
                            <th class="bold">ユーザーID</th>
                            <td>
                                <input type="text" name="" placeholder="<?= $current_user["nickname"]; ?>">
                            </td>
                        </tr>
                        <tr>
                            <th class="bold">性別</th>
                            <td>
                                <label>
                                    <input type="radio" name="" value="男性">男性</label>
                                <label>
                                    <input type="radio" name="" value="女性">女性</label>
                            </td>
                        </tr>
                        <tr>
                            <th class="bold">都道府県</th>
                            <td>
                                <div class="select_wrap">
                                    <select name="">
                                        <option value="" selected>選択して下さい</option>
                                        <option value="北海道">北海道</option>
                                        <option value="青森県">青森県</option>
                                        <option value="岩手県">岩手県</option>
                                        <option value="宮城県">宮城県</option>
                                        <option value="秋田県">秋田県</option>
                                        <option value="山形県">山形県</option>
                                        <option value="福島県">福島県</option>
                                        <option value="茨城県">茨城県</option>
                                        <option value="栃木県">栃木県</option>
                                        <option value="群馬県">群馬県</option>
                                        <option value="埼玉県">埼玉県</option>
                                        <option value="千葉県">千葉県</option>
                                        <option value="東京都">東京都</option>
                                        <option value="神奈川県">神奈川県</option>
                                        <option value="新潟県">新潟県</option>
                                        <option value="富山県">富山県</option>
                                        <option value="石川県">石川県</option>
                                        <option value="福井県">福井県</option>
                                        <option value="山梨県">山梨県</option>
                                        <option value="長野県">長野県</option>
                                        <option value="岐阜県">岐阜県</option>
                                        <option value="静岡県">静岡県</option>
                                        <option value="愛知県">愛知県</option>
                                        <option value="三重県">三重県</option>
                                        <option value="滋賀県">滋賀県</option>
                                        <option value="京都府">京都府</option>
                                        <option value="大阪府">大阪府</option>
                                        <option value="兵庫県">兵庫県</option>
                                        <option value="奈良県">奈良県</option>
                                        <option value="和歌山県">和歌山県</option>
                                        <option value="鳥取県">鳥取県</option>
                                        <option value="島根県">島根県</option>
                                        <option value="岡山県">岡山県</option>
                                        <option value="広島県">広島県</option>
                                        <option value="山口県">山口県</option>
                                        <option value="徳島県">徳島県</option>
                                        <option value="香川県">香川県</option>
                                        <option value="愛媛県">愛媛県</option>
                                        <option value="高知県">高知県</option>
                                        <option value="福岡県">福岡県</option>
                                        <option value="佐賀県">佐賀県</option>
                                        <option value="長崎県">長崎県</option>
                                        <option value="熊本県">熊本県</option>
                                        <option value="大分県">大分県</option>
                                        <option value="宮崎県">宮崎県</option>
                                        <option value="鹿児島県">鹿児島県</option>
                                        <option value="沖縄県">沖縄県</option>
                                    </select>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <button type="submit" class="btn_green center">変更内容を確認する</button>
            </form>
            
            <section>
                <h2><?= $current_user["nickname"]; ?>さんの相談</h2>
                <div class="my_question">
                    <p class="no_question text-center">質問した相談はありません</p>
                </div>
                <!-- my_question -->

                <div class="mypage_ban">
                    <div class="clearfix">
                        <div class="left clearfix">
                            <img src="/c9fdo/img/under/mypage_ban01.png" alt="無料で相談" class="left">
                            <img src="/c9fdo/img/under/mypage_ban02.png" alt="専門家が回答" class="left">
                        </div>
                        <div class="right text-center">
                            <strong>不動産のプロの<span>無料回答</span>でお悩み解決</strong>
                            <p>あなたの不動産に関するお悩みを無料で専門家が回答！<br>日本最大級の不動産相談Q&amp;Aサービス</p>
                            <a href="/c9fdo/questions/add" class="btn_orange center">不動産のお悩みを相談する</a>
                        </div>
                    </div>
                    <div class="clearfix">
                        <p class="left"><i class="fa fa-question-circle-o" aria-hidden="true"></i> ヘルプ</p>
                        <ul class="left clearfix">
                            <li class="left"><a href="/c9fdo/questions/under_construction">・よくあるお問い合わせ</a></li>
                            <li class="left"><a href="/c9fdo/questions/under_construction">・相談のしかた</a></li>
                        </ul>
                    </div>
                    
                </div>
                <!-- myapage_ban -->

                <form action="" method="get" class="clearfix mypage_search">
                    <input type="text" name="" placeholder="キーワードを入力してください" class="left">
                    <button type="submit" name="" class="right">みんなの相談を検索</button>
                </form>
            </section>


        </main>
        <aside class="right">
            <div class="side_mypage">
                <div class="clearfix side_my_account">
                    <img src="/c9fdo/img/under/side_my_thumb.png" alt="" class="left">
                    <p class="left bold"><?= $current_user["nickname"]; ?> <span>さん</span></p>
                    <a href="/c9fdo/users/logout" class="right">ログアウト</a>
                </div>
                <!-- side_account -->

                <div class="side_my_top">
                    <p class="bold"><a href="#"><i class="fa fa-user" aria-hidden="true"></i>マイページ トップ</a></p>
                </div>
                <!-- side_top -->

                <div class="side_my_every_consult">
                    <p class="bold"><a href="/c9fdo/questions/under_construction"><i class="fa fa-question-circle" aria-hidden="true"></i>みんなの相談</a></p>
                    <ul>
                        <li class="current"><a href="/c9fdo/questions/under_construction">自分の相談</a></li>
                        <li><a href="/c9fdo/questions/under_construction">回答した相談</a></li>
                        <li><a href="/c9fdo/questions/under_construction">お気に入り</a></li>
                    </ul>
                    <a href="/c9fdo/questions/add" class="btn_orange text-center"><img src="/c9fdo/img/under/side_q_icon_o.png" alt="">新しく相談する</a>

                </div>
                <!-- /.side_every_consult -->

                <div class="side_my_estimate">
                    <p class="bold"><a href="/c9fdo/questions/under_construction"><i class="fa fa-file-text" aria-hidden="true"></i>一括見積</a></p>
                    <ul>
                        <li><a href="/c9fdo/questions/under_construction">自分の一括見積依頼</a></li>
                        <li><a href="/c9fdo/questions/under_construction">一括見積のよくあるお問い合わせ</a></li>
                    </ul>
                    <a href="/c9fdo/questions/under_construction" class="btn_orange text-center"><img src="/c9fdo/img/under/estimate_icon.png" alt="">見積を依頼する</a>

                </div>
                <!-- /.side_my_estimate -->

                <div class="side_my_setting">
                    <p class="bold"><a href="/c9fdo/questions/under_construction"><i class="fa fa-cog" aria-hidden="true"></i>各種設定</a></p>
                    <ul>
                        <li><a href="/c9fdo/questions/under_construction">プロフィール情報の変更</a></li>
                        <li><a href="/c9fdo/questions/under_construction">メールアドレスの変更</a></li>
                    </ul>
                    <a href="/c9fdo/questions/under_construction" class="btn_orange text-center"><img src="/c9fdo/img/under/estimate_icon.png" alt="">見積を依頼する</a>

                </div>
                <!-- /.side_my_setting -->

                <div class="side_my_unsubscribed">
                    <a href="/c9fdo/questions/under_construction">不動産ドットコムのご退会</a>
                </div>
                <!-- /.side_my_setting -->
            </div>
        </aside>
    </div>