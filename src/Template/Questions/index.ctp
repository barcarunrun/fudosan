<?php
// debug($returnpage);
?>
  <!-- /top_info -->
  <div class="clearfix inner">
    <main class="left">
      <?php
        if(is_null($current_user)) {
          echo '
      <div class="no_login_ban pc">
        <a href="/questions/add" class="btn_green">ログインして相談する</a>
        <p>会員登録がお済みでない方は<br><a href="/c9fdo/users/register">無料会員登録ページ</a>へ</p>
      </div>
      <div class="sp sp_no_login_ban">
          <a href="/questions/add"><img src="img/top/sp_no_login_ban.png" alt="あなたのお悩みに不動産の専門家が回答" class="img_responsive"></a>
      </div>';
      } else {
        echo '
      <div class="login_ban pc">
        <a href="/questions/add" class="btn_green"><i class="fa fa-angle-right" aria-hidden="true"></i> 今すぐプロに無料相談</a>
      </div>
      <div class="sp sp_login_ban">
          <a href="/questions/add"><img src="img/top/sp_login_ban.png" alt="不動産のお悩みを気軽に質問してみよう" class="img_responsive"></a>
      </div>';
        };
      ?>
      
      
      <div class="keywords_search">
        <!--<?php// print_r($contents)?>-->
        <h2>キーワードで不動産に関するQ&amp;Aを探す</h2>
        <div>
          <?= $this->Form->create($keywords,['class'=>'clearfix']) ?>
          <?= $this->Form->control('title',['type'=>'text','placeholder'=>'キーワードを入力して下さい。','class'=>'clearfix']) ?>
          <?= '<button type="" class="right"><i class="fa fa-search" aria-hidden="true"></i>検索する</button>' ?>
          <!--<?//= $this->Form->button('ログイン',['class'=>'btn_green small width02']) ?>-->
          <?= $this->Form->end() ?>
          <!--<form action="" method="get" class="clearfix">-->
          <!--  <input type="text" name="" placeholder="キーワードを入力して下さい。" class="left">-->
          <!--  <button type="" class="right"><i class="fa fa-search" aria-hidden="true"></i>検索する</button>-->
          <!--</form>-->
          <p class="color_blue">例）ペット、楽器、境界紛争、敷金返還、バンド目指して上京、声優目指して上京、等。</p>
        </div>
      </div>
      <!-- /keywords_search -->
      <div class="category_search">
        <h2>カテゴリから不動産に関するQ&amp;Aを探す</h2>

        <div class="clearfix category_list" 
        style="
        display: -webkit-flex;
    display: flex;
    -webkit-flex-wrap: wrap;
    flex-wrap: wrap;
    -webkit-flex-direction: column;
    flex-direction: column;
    max-height: 2300px;
   
  ">
          <!--json作成用-->
          <!--<?php/*
          echo '{';
          for($a=1;$a<10;$a+=1){
            echo '"l' . $a . '": {<br>
              "name": "賃貸",<br>
              "icon": "category_icon01.png",<br>
              "description": "賃貸説明文。賃貸説明文。賃貸説明文。賃貸説明文。賃貸説明文。賃貸説明文。賃貸説明文。賃貸説明文。賃貸説明文。賃貸説明文。賃貸説明文。賃貸説明文。賃貸説明文。賃貸説明文。賃貸説明文。賃貸説明文。賃貸説明文。賃貸説明文。賃貸説明文。賃貸説明文。賃貸説明文。",' . '<br>' . '
              "middle_categories": {<br>';
            for($b=1;$b<7;$b+=1){
              echo '"m' . $a . $b . '": {<br>
                "name": "必要書類",<br>
                "img": "category_thumb_m11.png",<br>
                "description": "必要書類説明文。必要書類説明文。必要書類説明文。必要書類説明文。必要書類説明文。必要書類説明文。必要書類説明文。必要書類説明文。必要書類説明文。必要書類説明文。",<br>
                "small_categories": {<br>';
              for($c=1;$c<4;$c+=1){
                echo '"s' . $a . $b . $c . '": {<br>
                  "name": "住民票",<br>
                  "img": "category_thumb_s111.png",<br>
                  "description": "住民票説明文。住民票説明文。住民票説明文。住民票説明文。住民票説明文。住民票説明文。住民票説明文。住民票説明文。住民票説明文。住民票説明文。"<br>
                }';
                if($c<3){
                  echo ',';
                }else{
                  echo '}';
                }
              }
              echo '}';
              if($b<6){
                echo ',';
              }else{
                echo '}';
              }
            }
            echo '}';
            if($a<9){
              echo ',';
            }else{
              echo '}';
            }
          }*/
          ?>-->
          <?php $i=0; ?>  
          <?php foreach ((array)$contents as $category_l => $content):?>
        
            <div style="display:inline-block;" id="c<?php echo $i ?>">
          <div class="left">
            <p><a href="
            <?php if(empty($content->deploy_flag)){
              echo '/questions/under_construction">';
              }else{
                echo '/questions/inventory_l/' . $category_l . '">';  
              } ?>
            <?= $content->name ?></a></p>
            <div class="clearfix">
              <img src="img/top/<?= $content->icon ?>" alt="" class="left" style="width:38px;">
              <ul class="left clearfix">
                
                <?php foreach ($content->middle_categories as $category_m => $middle_category):?>
                <li><a href="
                <?php if(empty($content->deploy_flag)){
                  echo '/questions/under_construction">';
                  }else{
                    echo '/questions/inventory_l/' . $category_l . '#' . $category_m . '">';
                    // echo '/c9fdo/questions/inventory_m/' . $category_l . '/' . $category_m . '">';
                  } ?>
                <?= $middle_category->name ?></a></li>
                <?php endforeach; ?>
                
              </ul>
            </div>
          </div>
          </div>
          <?php $i=$i+1; ?>
          <?php endforeach; ?>
          
        </div>
      </div>


      <div class="estimate_ban pc">
        <a href="/questions/under_construction" class="btn_green">複数の専門家に一括見積り</a>
      </div>
      <div class="sp_estimate_ban sp">
          <a href="/questions/under_construction"><img src="img/top/sp_estimate_ban.png" alt="たった1回の入力で、複数の専門家からお見積可能です" class="img_responsive"></a>
      </div>
      <!-- /estimate_ban -->
      <!-- /category_search -->



      <link rel="stylesheet" href="./formdetail.css">
      <link rel="stylesheet" href="dest/css/expert-register.min.css">
      <link rel="stylesheet" href="/dest/css/expert-register.min.css">
      <link rel="stylesheet" href="css/dest/expert-register.min.css">
       <link rel="stylesheet" href="/css/dest/expert-register.min.css">

<div class="column_full clearfix" id="lawyerSearchBox">
    <div class="section clearfix">
    <p class=" ttl_bar_bold ttl_bar_slim search_detail--ttl">条件を指定して探す</p>
            
        
        <form id="lawyerSearch" name="lawyerSearch" action="https://www.bengo4.com/search/result/" method="post">
            <input type="hidden" value="e96612549638837fe94e01801406c1c4f3a51e4f" name="csrf">
            <div style="display:none;">
                <input name="city[]" disabled="disabled" class="modal_item" id="city" type="text"> <input name="gyosei[]"
                    disabled="disabled" class="modal_item" id="gyosei" type="text"> <input name="line[]" disabled="disabled"
                    class="modal_item" id="line" type="text"> <input name="station[]" disabled="disabled" class="modal_item"
                    id="station" type="text">
                <div id="createArea"></div>
            </div>
            
            <dl class="table_simple">
                <dt id="area_select">地域・路線</dt>
                <dd class="btns_area">
                    <select class="form--select_b" name="prefectureName" id="prefectureName">
                        <option value="0">選択してください。</option>
                        <option value="hokkaido">北海道</option>
                        <option value="aomori">青森県</option>
                        <option value="iwate">岩手県</option>
                        <option value="miyagi">宮城県</option>
                        <option value="akita">秋田県</option>
                        <option value="yamagata">山形県</option>
                        <option value="fukushima">福島県</option>
                        <option value="ibaraki">茨城県</option>
                        <option value="tochigi">栃木県</option>
                        <option value="gunma">群馬県</option>
                        <option value="saitama">埼玉県</option>
                        <option value="chiba">千葉県</option>
                        <option value="tokyo">東京都</option>
                        <option value="kanagawa">神奈川県</option>
                        <option value="yamanashi">山梨県</option>
                        <option value="nagano">長野県</option>
                        <option value="nigata">新潟県</option>
                        <option value="toyama">富山県</option>
                        <option value="ishikawa">石川県</option>
                        <option value="fukui">福井県</option>
                        <option value="gifu">岐阜県</option>
                        <option value="shizuoka">静岡県</option>
                        <option value="aichi">愛知県</option>
                        <option value="mie">三重県</option>
                        <option value="shiga">滋賀県</option>
                        <option value="kyoto">京都府</option>
                        <option value="osaka">大阪府</option>
                        <option value="hyogo">兵庫県</option>
                        <option value="nara">奈良県</option>
                        <option value="wakayama">和歌山県</option>
                        <option value="tottori">鳥取県</option>
                        <option value="shimane">島根県</option>
                        <option value="okayama">岡山県</option>
                        <option value="hiroshima">広島県</option>
                        <option value="yamaguchi">山口県</option>
                        <option value="tokushima">徳島県</option>
                        <option value="kagawa">香川県</option>
                        <option value="ehime">愛媛県</option>
                        <option value="kochi">高知県</option>
                        <option value="fukuoka">福岡県</option>
                        <option value="saga">佐賀県</option>
                        <option value="nagasaki">長崎県</option>
                        <option value="kumamoto">熊本県</option>
                        <option value="oita">大分県</option>
                        <option value="miyazaki">宮崎県</option>
                        <option value="kagoshima">鹿児島県</option>
                        <option value="okinawa">沖縄県</option>
                    </select>
                    <span class="hide js-btn_zenkokusearch btn_general btn_select_cancel fRight"><span class="icon_1x i_delete_g_1x"></span>選択を解除する</span>

                    <div id="area_tab" class="clearfix" style="display: none;">
                        <ul class="tab_navi navi3">
                            <li class="selected"><span class="btn_general btn_area btn_toggle_container" id="tab_prefecture"><span
                                        class="icon_1x i_area2_1x"></span>地域で探す</span></li>
                            <li class=""><span class="btn_general btn_line btn_toggle_container" id="tab_train"><span
                                        class="icon_1x i_line_1x"></span>路線で探す</span></li>
                        </ul>
                        <div class="container_tab tab_selected clearfix">
                            <span class="btn_general btn_show_modal fLeft" data-type="prefecture-1" data-page="top"><span
                                    class="icon_1x i_area2_1x"></span>地域で絞り込む</span>

                            <div class="fLeft selected_items">
                                <span class="selected-city fLeft"></span>
                            </div>
                            <div class="fRight">
                                <span class="btn_general btn_check_cancel" data-type="train"><span class="icon_1x i_delete_g_1x"></span>選択を解除する</span>
                            </div>
                        </div>
                        <div class="container_tab container_hide clearfix">
                            <span class="btn_general btn_show_modal fLeft" data-type="train-1" data-page="top"><span
                                    class="icon_1x i_line_1x"></span>路線で絞り込む</span>

                            <div class="fLeft selected_items">
                                <span class="selected-station fLeft"></span>
                            </div>
                            <div class="fRight">
                                <span class="btn_general btn_check_cancel" data-type="prefecture"><span class="icon_1x i_delete_g_1x"></span>選択を解除する</span>
                            </div>
                        </div>
                    </div>

                </dd>
                <dt>分野</dt>
                <dd>
                    <ul class="list_item">
                        <li class="inline-block">
                            <label class="form--checkbox">
                                <input id="fieldId" name="fieldId" value="1" class="radio" data-value="fieldId_1-path1_0"
                                    type="radio"> <span class="txt_radio">賃貸（居住用・店舗事務所）</span>
                            </label>
                        </li>
                        <li class="inline-block">
                            <label class="form--checkbox">
                                <input id="fieldId" name="fieldId" value="2" class="radio" data-value="fieldId_2-path1_0"
                                    type="radio"> <span class="txt_radio">売買</span>
                            </label>
                        </li>
                        <li class="inline-block">
                            <label class="form--checkbox">
                                <input id="fieldId" name="fieldId" value="3" class="radio" data-value="fieldId_3-path1_0"
                                    checked="checked" type="radio"> <span class="txt_radio">不動産（住宅）ローン全般</span>
                            </label>
                        </li>
                        <li class="inline-block">
                            <label class="form--checkbox">
                                <input id="fieldId" name="fieldId" value="4" class="radio" data-value="fieldId_4-path1_0"
                                    type="radio"> <span class="txt_radio">保険</span>
                            </label>
                        </li>
                        <li class="inline-block">
                            <label class="form--checkbox">
                                <input id="fieldId" name="fieldId" value="5" class="radio" data-value="fieldId_5-path1_0"
                                    type="radio"> <span class="txt_radio">管理</span>
                            </label>
                        </li>
                        <li class="inline-block">
                            <label class="form--checkbox">
                                <input id="fieldId" name="fieldId" value="6" class="radio" data-value="fieldId_6-path1_0"
                                    type="radio"> <span class="txt_radio">不動産投資（国内）</span>
                            </label>
                        </li>
                        <li class="inline-block">
                            <label class="form--checkbox">
                                <input id="fieldId" name="fieldId" value="7" class="radio" data-value="fieldId_7-path1_0"
                                    type="radio"> <span class="txt_radio">海外不動産投資（海外）</span>
                            </label>
                        </li>
                        <li class="inline-block">
                            <label class="form--checkbox">
                                <input id="fieldId" name="fieldId" value="8" class="radio" data-value="fieldId_8-path1_0"
                                    type="radio"> <span class="txt_radio">リフォーム</span>
                            </label>
                        </li>
                        <li class="inline-block">
                            <label class="form--checkbox">
                                <input id="fieldId" name="fieldId" value="9" class="radio" data-value="fieldId_9-path1_0"
                                    type="radio"> <span class="txt_radio">ハウスクリーニング</span>
                            </label>
                        </li>
                        <li class="inline-block">
                            <label class="form--checkbox">
                                <input id="fieldId" name="fieldId" value="10" class="radio" data-value="fieldId_10-path1_0"
                                    type="radio"> <span class="txt_radio">競売</span>
                            </label>
                        </li>
                        <li class="inline-block">
                            <label class="form--checkbox">
                                <input id="fieldId" name="fieldId" value="11" class="radio" data-value="fieldId_11-path1_0"
                                    type="radio"> <span class="txt_radio">不動産相続</span>
                            </label>
                        </li>
                        <li class="inline-block">
                            <label class="form--checkbox">
                                <input id="fieldId" name="fieldId" value="12" class="radio" data-value="fieldId_12-path1_0"
                                    type="radio"> <span class="txt_radio">引越し</span>
                            </label>
                        </li>
                        <li class="inline-block">
                            <label class="form--checkbox">
                                <input id="fieldId" name="fieldId" value="13" class="radio" data-value="fieldId_13-path1_0"
                                    type="radio"> <span class="txt_radio">ゴミ捨て</span>
                            </label>
                        </li>
                        <li class="inline-block">
                            <label class="form--checkbox">
                                <input id="fieldId" name="fieldId" value="14" class="radio" data-value="fieldId_14-path1_0"
                                    type="radio"> <span class="txt_radio">離婚による不動産関連全般</span>
                            </label>
                        </li>
                         <li class="inline-block">
                      <label class="form--checkbox">
                        <input id="fieldId" name="fieldId" value="15" class="radio" data-value="fieldId_15-path1_0"
                               type="radio"> <span class="txt_radio">駐車場</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input id="fieldId" name="fieldId" value="16" class="radio" data-value="fieldId_16-path1_0"
                               type="radio"> <span class="txt_radio">民泊・シェアハウス・ゲストハウス</span>
                      </label>
                    </li >
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input id="fieldId" name="fieldId" value="17" class="radio" data-value="fieldId_17-path1_0"
                               type="radio"> <span class="txt_radio">不動産トラブル</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input id="fieldId" name="fieldId" value="18" class="radio" data-value="fieldId_18-path1_0"
                               type="radio"> <span class="txt_radio">地名や構造物から、安全or危険地域を見抜く方法</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input id="fieldId" name="fieldId" value="19" class="radio" data-value="fieldId_19-path1_0"
                               type="radio"> <span class="txt_radio">風水や、四柱推命等の占いの有効性について</span>
                      </label>
                    </li>
                    </ul>
                </dd>
                <dt class="category_items">料金表・事例</dt>
                <dd class="category_items">
                    <ul class="list_item">
                        <li class="inline-block">
                            <label class="form--checkbox">
                                <input id="price1" name="price" class="checkbox" value="1" type="checkbox"> <span class="txt_checkbox">料金表あり</span>
                                
                            </label>
                        </li>
                        <li class="inline-block">
                            <label class="form--checkbox">
                                <input id="case1" name="case" class="checkbox" value="1" type="checkbox"> <span class="txt_checkbox">解決事例あり</span>
                              
                            </label>
                        </li>
                    </ul>
                </dd>
                <dt class="category_items">対応体制</dt>
                <dd class="category_items">
                    <ul class="list_item">
                        <li class="inline-block">
                            <label class="form--checkbox">
                                <input name="iconExpertAd[]" class="checkbox" separator="" value="8" id="iconExpertAd8"
                                    type="checkbox"> <span class="txt_checkbox">全国出張対応</span>
                                
                            </label>
                            
                        </li>
                        <li class="inline-block">
                            <label class="form--checkbox">
                                <input name="iconExpertAd[]" class="checkbox" separator="" value="9" id="iconExpertAd9"
                                    type="checkbox"> <span class="txt_checkbox">24時間予約受付</span>
                                
                            </label>
                        </li>
                        <li class="inline-block">
                            <label class="form--checkbox">
                                <input name="iconExpertAd[]" class="checkbox" separator="" value="10" id="iconExpertAd10"
                                    type="checkbox"> <span class="txt_checkbox">女性スタッフ在籍</span>
                                
                            </label>
                           
                        </li>
                        <li class="inline-block">
                            <label class="form--checkbox">
                                <input name="iconExpertAd[]" class="checkbox" separator="" value="11" id="iconExpertAd11"
                                    type="checkbox"> <span class="txt_checkbox">当日相談可</span>
                                
                            </label>
                        </li>
                        <li class="inline-block">
                            <label class="form--checkbox">
                                <input name="iconExpertAd[]" class="checkbox" separator="" value="12" id="iconExpertAd12"
                                    type="checkbox"> <span class="txt_checkbox">休日相談可</span>
                               
                            </label>
                        </li>
                        <li class="inline-block">
                            <label class="form--checkbox">
                                <input name="iconExpertAd[]" class="checkbox" separator="" value="15" id="iconExpertAd15"
                                    type="checkbox"> <span class="txt_checkbox">夜間相談可</span>
                                
                            </label>
                            
                        </li>
                        <li class="inline-block">
                            <label class="form--checkbox">
                                <input name="iconExpertAd[]" class="checkbox" separator="" value="16" id="iconExpertAd16"
                                    type="checkbox"> <span class="txt_checkbox">電話相談可</span>
                                
                            </label>
                        </li>
                    </ul>
                </dd>
                <dt class="category_items">お支払い方法</dt>
                <dd class="category_items">
                    <ul class="list_item">
                        
                        <li class="inline-block">
                            <label class="form--checkbox">
                                <input name="iconExpertAd[]" class="checkbox" separator="" value="7" id="iconExpertAd7"
                                    type="checkbox"> <span class="txt_checkbox">初回相談無料</span>
                                
                            </label>
                           
                        </li>
                        <li class="inline-block">
                            <label class="form--checkbox">
                                <input name="iconExpertAd[]" class="checkbox" separator="" value="17" id="iconExpertAd17"
                                    type="checkbox"> <span class="txt_checkbox">分割払いあり</span>
                                
                            </label>
                        </li>
                        <li class="inline-block">
                            <label class="form--checkbox">
                                <input name="iconExpertAd[]" class="checkbox" separator="" value="18" id="iconExpertAd18"
                                    type="checkbox"> <span class="txt_checkbox">後払いあり</span>
                                
                            </label>
                        </li>
                      
                        <li class="inline-block">
                            <label class="form--checkbox">
                                <input name="iconExpertAd[]" class="checkbox" separator="" value="21" id="iconExpertAd21"
                                    type="checkbox"> <span class="txt_checkbox">カード払いあり</span>
                                
                            </label>
                        </li>
                    </ul>
                </dd>
            </dl>
            <div style="padding-top:20px;">
            <p class=" ttl_bar_bold ttl_bar_slim search_detail--ttl">さらに細かく<span class="search_detail--ttl_strong">こだわり条件</span>を指定する</p>
            <dl class="table_simple">
                <dt><label for="LawyerSearch_sex">性別</label></dt>
                <dd>
                    <select class="form--select" name="sex" id="sex">
                        <option value="0">指定無し</option>
                        <option value="1">男性</option>
                        <option value="2">女性</option>
                    </select> </dd>
                <dt>年齢</dt>
                <dd>
                    <select class="form--select" name="age" id="age">
                        <option value="0">指定無し</option>
                        <option value="1">20代</option>
                        <option value="2">30代</option>
                        <option value="3">40代</option>
                        <option value="4">50代</option>
                        <option value="5">60代以上</option>
                    </select> </dd>
                <dt>交通アクセス</dt>
                <dd>
                    <dl>
                        <dt>駅徒歩</dt>
                        <dd>
                            <select name="walk" class="form--select" id="walk">
                                <option value="0">指定無し</option>
                                <option value="5">5分以内</option>
                                <option value="10">10分以内</option>
                                <option value="15">15分以内</option>
                            </select> </dd>
                        <dt>駐車場</dt>
                        <dd>
                            <ul class="list_item">
                                <li class="inline-block">
                                    <label class="form--checkbox">
                                        <input name="iconLawFirm[]" class="checkbox" value="1" id="iconLawFirm1" type="checkbox">
                                        <span class="txt_checkbox">駐車場あり</span>
        
                                    </label>
                                </li>
                                <li class="inline-block">
                                    <label class="form--checkbox">
                                        <input name="iconLawFirm[]" class="checkbox" value="2" id="iconLawFirm2" type="checkbox">
                                        <span class="txt_checkbox">駐車場近く</span>
                        
                                    </label>
                                </li>
                            </ul>
                        </dd>
                    </dl>
                </dd>
                <dt>設備</dt>
                <dd>
                    <ul class="list_item">
                        <li class="inline-block">
                            <label class="form--checkbox">
                                <input name="iconLawFirm[]" class="checkbox" value="3" id="iconLawFirm3" type="checkbox">
                                <span class="txt_checkbox">完全個室で相談</span>

                            </label>
                        </li>
                        <li class="inline-block">
                            <label class="form--checkbox">
                                <input name="iconLawFirm[]" class="checkbox" value="4" id="iconLawFirm4" type="checkbox">
                                <span class="txt_checkbox">託児所・キッズルーム</span>
        
                            </label>
                        </li>
                        <li class="inline-block">
                            <label class="form--checkbox">
                                <input name="iconLawFirm[]" class="checkbox" value="5" id="iconLawFirm5" type="checkbox">
                                <span class="txt_checkbox">バリアフリー</span>
            
                            </label>
                           
                        </li>
                    </ul>
                </dd>
                <dt>対応言語</dt>
                <dd>
                    <ul class="list_item">
                        <li class="inline-block">
                            <label class="form--checkbox">
                                <input name="iconLawFirm[]" class="checkbox" value="38" id="iconLawFirm38" type="checkbox">
                                <span class="txt_checkbox">英語</span>
                        
                            </label>
                        </li>
                        <li class="inline-block">
                            <label class="form--checkbox">
                                <input name="iconLawFirm[]" class="checkbox" value="39" id="iconLawFirm39" type="checkbox">
                                <span class="txt_checkbox">中国語</span>
        
                            </label>
                        </li>
                        <li class="inline-block">
                            <label class="form--checkbox">
                                <input name="iconLawFirm[]" class="checkbox" value="40" id="iconLawFirm40" type="checkbox">
                                <span class="txt_checkbox">韓国語（朝鮮語）</span>
                
                            </label>
                        </li>
                        <li class="inline-block">
                            <label class="form--checkbox">
                                <input name="iconLawFirm[]" class="checkbox" value="41" id="iconLawFirm41" type="checkbox">
                                <span class="txt_checkbox">フランス語</span>
                
                            </label>
                        </li>
                        <li class="inline-block">
                            <label class="form--checkbox">
                                <input name="iconLawFirm[]" class="checkbox" value="42" id="iconLawFirm42" type="checkbox">
                                <span class="txt_checkbox">スペイン語</span>
            
                            </label>
                        </li>
                        <li class="inline-block">
                            <label class="form--checkbox">
                                <input name="iconLawFirm[]" class="checkbox" value="43" id="iconLawFirm43" type="checkbox">
                                <span class="txt_checkbox">ポルトガル語</span>
                            
                            </label>
                        </li>
                        <li class="inline-block">
                            <label class="form--checkbox">
                                <input name="iconLawFirm[]" class="checkbox" value="44" id="iconLawFirm44" type="checkbox">
                                <span class="txt_checkbox">ドイツ語</span>
                    
                            </label>
                        </li>
                    </ul>
                </dd>
               
                <dt>資格</dt>
                <dd>
               <ul class="list_item">
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">宅地建物取引士</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">マンション管理士</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">業務管理主任者</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">ビル経営管理士</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">賃貸不動産経営管理士</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">区分所有管理士</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">認定ファシリティマネージャー</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">不動産鑑定士</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">土地家屋調査士</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">司法書士</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">弁護士</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">海外法曹資格</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">公認会計士</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">税理士</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">住宅ローンアドバイザー</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">モーゲージプランナー</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">土地改良換地士</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">土地区画整理士</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">一級建築士</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">構造設計一級建築士</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">設備設計一級建築士</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">二級建築士</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">木造建築士</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">管理建築士</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">測量士</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">消防設備士</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">第一種電気工事士</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">第二種電気工事士</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">甲種・防火管理者</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">乙種・防火管理者</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">インテリアコーディネーター</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">不動産コンサルティングマスター</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">不動産証券化協会認定マスター</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">不動産カウンセラー</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">再開発プランナー</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">ホームインスペクター（住宅診断士）</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">不動産戦略アドバイザー</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">太陽光発電アドバイザー</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">任意売却取扱責任者</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">住宅建築コーディネーター</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">競売不動産取扱主任者</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">シックハウス診断士</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">行政書士</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">ファイナンシャルプランナー</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">金融機関の融資担当者</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">社会保険労務士</span>
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="" id="iconLawyer" type="checkbox">
                        <span class="txt_checkbox">鍵（カギ）業者</span>
                      </label>
                    </li>  
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="30" id="iconLawyer30" type="checkbox">
                        <span class="txt_checkbox">公認会計士</span>
                        
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="31" id="iconLawyer31" type="checkbox">
                        <span class="txt_checkbox">医師・薬剤師</span>
                        
                      </label>
                     
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="32" id="iconLawyer32" type="checkbox">
                        <span class="txt_checkbox">不動産鑑定士・宅建</span>
                       
                      </label>
                     
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="33" id="iconLawyer33" type="checkbox">
                        <span class="txt_checkbox">一級・二級建築士</span>
                       
                      </label>
                      
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="34" id="iconLawyer34" type="checkbox">
                        <span class="txt_checkbox">IT国家資格</span>
                       
                      </label>
                      
                
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="35" id="iconLawyer35" type="checkbox">
                        <span class="txt_checkbox">海外法曹資格</span>
                       
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="36" id="iconLawyer36" type="checkbox">
                        <span class="txt_checkbox">中小企業診断士</span>
                        
                      </label>
                    </li>
                    <li class="inline-block">
                      <label class="form--checkbox">
                        <input name="iconLawyer[]" class="checkbox" value="37" id="iconLawyer37" type="checkbox">
                        <span class="txt_checkbox">一級FP・CFP</span>
                        
                      </label>
                      <span class="popupbtn">
                        <span class="trigger i_help sprite2"></span>
                        <span class="popup" style="opacity: 0;">一級ファイナンシャルプランナー（FP）技能士またはCFPのどちらかの資格を保有している弁護士です。</span>
                      </span>
                    </li>
                  </ul>
                </dd>
              
            </dl>
            </div>
        </form>
    </div>
</div>
      
      <!-- /area_search -->

    
      

      <div class="top_news" style="padding-top:15px;">
        <h2>不動産ドットコムニュース</h2>
        <div class="clearfix">
          <div class="left">
            <a href="/c9fdo/questions/under_construction"><img src="img/top/dummy_news_thumb01.png" alt="" class="img_responsive">
            <p>首都圏中古マンション価格引き続き上昇</a></p>
          </div>
          <div class="left">
            <a href="/c9fdo/questions/under_construction"><img src="img/top/dummy_news_thumb02.png" alt="" class="img_responsive">
            <p>未婚女性の住宅購入理由、トップは「家賃を払い続けるのがも…</a></p>
          </div>
          <div class="left">
            <a href="/c9fdo/questions/under_construction"><img src="img/top/dummy_news_thumb03.png" alt="" class="img_responsive">
            <p>三鷹駅直結タワーマンション販売相鉄不動産と三菱地所レジ…</a></p>
          </div>
          <div class="left">
            <a href="/c9fdo/questions/under_construction"><img src="img/top/dummy_news_thumb04.png" alt="" class="img_responsive">
            <p>11月の首都圏マンション市場、新規発売戸数は3ヶ月ぶりに…</a></p>
          </div>
          <div class="left">
            <a href="/c9fdo/questions/under_construction"><img src="img/top/dummy_news_thumb05.png" alt="" class="img_responsive">
            <p>11月の分譲マンション賃料、首都圏は3ヵ月連続上昇、東京…</a></p>
          </div>
          <div class="left">
            <a href="/c9fdo/questions/under_construction"><img src="img/top/dummy_news_thumb01.png" alt="" class="img_responsive">
            <p>11月の近畿圏マンション市場、契約率は77.7%と引き続…</a></p>
          </div>
        </div>

        <div class="text-center">
          <a href="/c9fdo/questions/under_construction" class="btn_border">ニュースをもっと見る</a>
        </div>
        
      </div>
      <!-- /.top_news -->

      <div class="top_consultation">
        <h2>新着不動産相談</h2>
        <ul>
          <?php foreach ($results as $result):?>
          <li><a href="/c9fdo/questions/view/<?= $result["id"] . '">' .  $this->shorthand($result["title"],31); ?></a></li>
          <!--<li><a href="/c9fdo/questions/under_construction"><?//= $result["title"] ?></a></li>-->
          
          <?php endforeach; ?>
        </ul>
        <div class="text-center">
          <a href="/c9fdo/questions/search" class="btn_border">相談をもっと見る</a>
        </div>
      </div>
      <!-- /.top_consultation-->
    </main>



