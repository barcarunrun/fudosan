<div class="clearfix inner">
        <main class="left">
            <section>
                <h2><i class="fa fa-search" aria-hidden="true"></i>「<?= $current_category->name ?>」によせられる相談</h2>
                <p class="paginate_result_num"><?= $this->Paginator->counter(['format' => __('{{count}} 件みつかりました {{start}} - {{end}} 件目')]) ?></p>
                <?php foreach ($results as $result):?>
                <article class="search_ar_box">
                    <p class="search_ar_tit bold"><a href="/c9fdo/questions/view/<?= $result['id'] . '">' . $this->shorthand($result['title'],60); ?></a></p>
                    
                    <div>
                        <ul>
                            
                            <li><?= $current_category->parent->name ?></li>
                            <li><?= $current_category->name ?></li>
                        </ul>
                        <p><?= $this->shorthand($result['body'],144); ?></p>
                        <div class="clearfix">
                            <time class="left"><?php $created_date = new DateTime($result["created"]);echo $created_date->format('Y年m月d日'); ?></time>
                            <p class="expert_answer_count left">専門家回答<span>4</span>件</p>
                        </div>
                    </div>
                </article>
                <?php endforeach; ?>
                
                <p class="paginate_result_num"><?= $this->Paginator->counter(['format' => __('{{count}} 件みつかりました {{start}} - {{end}} 件目')]) ?></p>
                <div class="paginator">
                  <?= $this->Paginator->first('<<'); ?>
                  <?= $this->Paginator->prev('<'); ?>
                  <?= $this->Paginator->numbers(); ?>
                  <?= $this->Paginator->next('>'); ?>
                  <?= $this->Paginator->last('>>'); ?>
                </div>
                
                <!--<div class="text-center list_more">-->
                <!--    <a href="/c9fdo/questions/under_construction" class="btn_border"><?//= $current_category->name ?> の相談をもっと見る</a>-->
                <!--</div>-->
            </section>
            <section class="category_expert">
                <h2>「<?= $current_category->name ?>」の問題を扱う専門家</h2>
                <div class="expert_box">
                    <div class="expert_box_profile clearfix">
                        <img src="/c9fdo/img/under/expert_thumb.png" alt="" class="left">
                        <div class="left">
                            <p class="bold">山本太郎<span>司法書士</span></p>
                            <p><i class="fa fa-building-o" aria-hidden="true"></i>株式会社東京千代田司法書士オフィス</p>
                            <address><i class="fa fa-map-marker" aria-hidden="true"></i>東京都千代田区九段南1-2-1千代田ビル2F</address>
                            <ul>
                                <li class="bold"><i class="fa fa-volume-control-phone" aria-hidden="true"></i>03-333-3333</li>
                                <li class="bold"><a href="/c9fdo/questions/under_construction"><i class="fa fa-envelope" aria-hidden="true"></i>メールで面談予約</a></li>
                            </ul>
                        </div>
                        <div class="right">
                            <button class="btn_bookmark bold"><i class="fa fa-star" aria-hidden="true"></i>お気に入りに追加</button>
                        </div>
                    </div>
                    <div class="expert_box_desc clearfix">
                        <div class="clearfix">
                            <p class="speciality left">得意分野</p>
                            <p class="left bold">賃貸・相続</p>
                            <a href="/c9fdo/questions/under_construction" class="right">> この専門家の回答事例を見る</a>
                        </div>
                        <div>
                            <ul>
                                <li class="bold">休日相談可</li>
                                <li class="bold">当日相談可</li>
                                <li class="bold">当日相談可</li>
                            </ul>
                            <p>専門家プロフィールテキスト専門家プロフィールテキスト専門家プロフィールテキスト専門家プロフィールテキスト専門家プロフィールテキスト専門家プロフィールテキスト専門家プロフィールテキスト</p>
                        </div>
                    </div>
                </div>
                <div class="expert_box">
                    <div class="expert_box_profile clearfix">
                        <img src="/c9fdo/img/under/expert_thumb.png" alt="" class="left">
                        <div class="left">
                            <p class="bold">山本太郎<span>司法書士</span></p>
                            <p><i class="fa fa-building-o" aria-hidden="true"></i>株式会社東京千代田司法書士オフィス</p>
                            <address><i class="fa fa-map-marker" aria-hidden="true"></i>東京都千代田区九段南1-2-1千代田ビル2F</address>
                            <ul>
                                <li class="bold"><i class="fa fa-volume-control-phone" aria-hidden="true"></i>03-333-3333</li>
                                <li class="bold"><a href="/c9fdo/questions/under_construction"><i class="fa fa-envelope" aria-hidden="true"></i>メールで面談予約</a></li>
                            </ul>
                        </div>
                        <div class="right">
                            <button class="btn_bookmark bold"><i class="fa fa-star" aria-hidden="true"></i>お気に入りに追加</button>
                        </div>
                    </div>
                    <div class="expert_box_desc clearfix">
                        <div class="clearfix">
                            <p class="speciality left">得意分野</p>
                            <p class="left bold">賃貸・相続</p>
                            <a href="/c9fdo/questions/under_construction" class="right">> この専門家の回答事例を見る</a>
                        </div>
                        <div>
                            <ul>
                                <li class="bold">休日相談可</li>
                                <li class="bold">当日相談可</li>
                                <li class="bold">当日相談可</li>
                            </ul>
                            <p>専門家プロフィールテキスト専門家プロフィールテキスト専門家プロフィールテキスト専門家プロフィールテキスト専門家プロフィールテキスト専門家プロフィールテキスト専門家プロフィールテキスト</p>
                        </div>
                    </div>
                </div>
                <div class="text-center list_more">
                    <a href="/c9fdo/questions/under_construction" class="btn_border"><?= $current_category->name ?> 分野に注力する専門家（総勢216名）をもっと見る</a>
                </div>
            </section>
        </main>
        <aside class="right">
            <div class="side_expart_search">
                <h3><?= $current_category->name ?> に詳しい専門家を探す</h3>
                <div>
                    <a href="/c9fdo/questions/under_construction" class="btn_green text-center"><img src="/c9fdo/img/under/side_expart_icon.png" alt="">不動産専門家を探す</a>
                </div>
            </div>
            <div class="side_faq">
                <h3>よくある相談</h3>
                <div>
                    <ul class="clearfix">
                        <?php foreach ($current_category->small_categories as $category_s => $content_s):?>
                        <li><a href="/c9fdo/questions/inventory_s/<?= $category_l . '/' . $category_m . '/' . $category_s . '">' . $content_s->name ?></a></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
            <div class="related_category">
                <h3>関連カテゴリから探す</h3>
                <div>
                    <ul>
                        <li class="current"><span><?= $current_category->parent->name ?></span>
                            <ul>
                                <li class="current"><span><?= $current_category->name ?></span>
                                    <ul>
                                        <?php foreach ($current_category->small_categories as $category_s => $content_s):?>
                                        <li><a href="/c9fdo/questions/inventory_s/<?= $category_l . '/' . $category_m . '/'  . $category_s . '">' . '>' . $content_s->name?></a></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="side_estimate">
                <h3>不動産の専門家に見積もり依頼をする</h3>
                <div>
                    <a href="/c9fdo/questions/under_construction" class="btn_orange text-center"><img src="/c9fdo/img/under/estimate_icon.png" alt="">今すぐ簡単一括見積</a>
                </div>
            </div>
            <div class="side_news">
                <h3><?= $current_category->name ?> に関するニュース</h3>
                <ul>
                    <li><a href="/c9fdo/questions/under_construction">首都圏中古マンション価格、引き続き上昇</a></li>
                    <li><a href="/c9fdo/questions/under_construction">未婚女性の住宅購入理由、トップは「家賃を払い続けるのがも…</a></li>
                    <li><a href="/c9fdo/questions/under_construction">三鷹駅直結タワーマンション販売、相鉄不動産と三菱地所レジ…</a></li>
                    <li><a href="/c9fdo/questions/under_construction">11月の首都圏マンション市場、新規発売戸数は3ヶ月ぶりに…</a></li>
                </ul>
                <div class="text-center">
                    <a href="/c9fdo/questions/under_construction" class="btn_border">＋ 続きをみる</a>
                </div>
            </div>
            <!-- /side_news -->
        </aside>
    </div>