<div class="clearfix inner">
        <main class="left">
            <ul class="bread">
                <li><a href="/c9fdo/index">不動産ドットコムTOP</a></li>
                <li><a href="">相談</a></li>
                <li>検索結果「新着」</li>
            </ul>
            <section>
                <h2><i class="fa fa-search" aria-hidden="true"></i>「新着」の相談</h2>
                <p class="search_result_summary">「新着」に関する不動産相談です。リフォーム分野に、「新着」に関連する相談が多くよせられています。</p>
                
                <div class="search_result_inventory">
                <p class="paginate_result_num"><?= $this->Paginator->counter(['format' => __('{{count}} 件みつかりました {{start}} - {{end}} 件目')]) ?></p>
                <?php foreach ($results as $result):?>
                <article class="search_ar_box">
                    <p class="search_ar_tit bold"><a href="/c9fdo/questions/view/<?= $result['id'] . '">' . $this->shorthand($result['title'],60); ?></a></p>
                    <div>
                        <ul>
                            <li><?= $this->get_category('l' . $result['large_category_id'])->name ?></li>
                            <li><?= $this->get_category('l' . $result['large_category_id'],'m' . $result['middle_category_id'])->name ?></li>
                            <li><?= $this->get_category('l' . $result['large_category_id'],'m' . $result['middle_category_id'],'s' . $result['small_category_id'])->name ?></li>
                        </ul>
                        <p><?= $this->shorthand($result['body'],144); ?></p>
                        <div class="clearfix">
                            <time class="left"><?php $created_date = new DateTime($result["created"]);echo $created_date->format('Y年m月d日'); ?></time>
                            <p class="expert_answer_count left">専門家回答<span>4</span>件</p>
                        </div>
                    </div>
                </article>
                <?php endforeach; ?>
                
                <p class="paginate_result_num"><?= $this->Paginator->counter(['format' => __('{{count}} 件みつかりました {{start}} - {{end}} 件目')]) ?></p>
                </div>
                <div class="paginator">
                <!--<div class="pager">-->
                  <?= $this->Paginator->first('<<'); ?>
                  <?= $this->Paginator->prev('<'); ?>
                  <?= $this->Paginator->numbers(); ?>
                  <?= $this->Paginator->next('>'); ?>
                  <?= $this->Paginator->last('>>'); ?>
                </div>
                
                <!--<p class="search_hit_count bold">158件見つかりました。 11-20件目</p>-->
                <!--<?php-->
                <!--//for($i=0;$i<10;$i+=1){echo'-->
                <!--<article class="search_ar_box">-->
                <!--    <p class="search_ar_tit bold"><a href="/c9fdo/questions/view">相談タイトル</a></p>-->
                <!--    <div>-->
                <!--        <ul>-->
                <!--            <li>相続</li>-->
                <!--            <li>遺産分割</li>-->
                <!--            <li>相続財産</li>-->
                <!--        </ul>-->
                <!--        <p>相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章相談文章・・・</p>-->
                <!--        <div class="clearfix">-->
                <!--            <time class="left">2018年01月01日</time>-->
                <!--            <p class="expert_answer_count left">専門家回答<span>4</span>件</p>-->
                <!--        </div>-->
                <!--    </div>-->
                <!--</article>-->
                <!--';} ?>-->

                <!--<p class="search_hit_count bold">158件見つかりました。 11-20件目</p>-->
            </section>
            <!--<div class="pager">-->
            <!--    <ul>-->
            <!--        <li>-->
            <!--            <a href="#">-->
            <!--                <</a>-->
            <!--        </li>-->
            <!--        <li><a href="#">1</a></li>-->
            <!--        <li class="current"><span>2</span></li>-->
            <!--        <li><a href="#">3</a></li>-->
            <!--        <li><a href="#">4</a></li>-->
            <!--        <li><span>...</span></li>-->
            <!--        <li><a href="#">16</a></li>-->
            <!--        <li><a href="#">></a></li>-->
            <!--        <li><a href="#">>></a></li>-->
            <!--    </ul>-->
            <!--</div>-->
        </main>
