<?php
//var_dump($_POST);
// debug($contents);
$page_flag = 0;
if( !empty($_POST['confirm']) ) {
	$page_flag = 1;
}elseif (!empty($_POST['submit'])) {
    $page_flag = 2; //完了画面
} else {
    $page_flag = 0; //入力画面
}
?>

<main class="inner">
        <ul class="bread">
            <li><a href="/c9fdo/index">不動産ドットコムTOP</a></li>
            <!--<li><a href="/c9fdo/questions/under_construction">不動産相続</a></li>-->
            <li>新規相談の投稿</li>
        </ul>
        <article>
            <div class="estimate_ban02 pc">
                <a href="/c9fdo/questions/under_construction" class="btn_white bold">一括見積について詳しく見る</a>
            </div>
            <!-- /estimate_ban -->
            <div class="sp_estimate_ban02 sp">
                <a href="/c9fdo/questions/under_construction"><img src="/c9fdo/img/under/sp_estimate_ban02.png" alt="簡単入力で完全無料！複数の不動産の専門家に一括見積請求" class="img_responsive"></a>
            </div>
            <!-- /estimate_ban -->
            <p class="copy bold"><span>みんなの不動産相談</span>｜無料で優良不動産業者・専門家が回答する日本最大級の「不動産に関するQ＆Aサイト」</p>
            
            <?php if( $page_flag === 1 ): ?>
            
            <ol class="stepbar step3">
                <li class="step">①相談内容の入力</li>
                <li class="step current">②カテゴリの選択・相談内容の確認</li>
                <li class="step">③相談完了</li>
            </ol>
            
            <div class="question_form_box">
            <form method="post" accept-charset="utf-8" action="/questions/add" >
                <h2>相談カテゴリの選択</h2>
                <p class="select_notes">相談の内容に適していると思われるカテゴリを選択してください。</p>
                <p class="select_notes">相談の内容に適したカテゴリへ投稿することにより、専門家回答がつきやすくなります。</p>
                <h3 class="category_select_heading">カテゴリ候補<span class="must">必須</span></h3>
                <div class="category_select">
                    <div class="">
                		<label>大カテゴリ</label>
                		<select class="parent" name="large_category_id" required="required">
                          <option value="" selected="selected">選択してください</option>
                    		<?php foreach ($contents as $large_category_id => $content): ?>
                    		  <?= '<option value="' . substr($large_category_id,1) . '">'; ?>
                    		  <?= $content->name . '</option>'; ?>
                        	  <!--<p><?//= $this->Form->control('large_category_id',['type'=>'select','options' => $large_categories,'class'=>'name']); ?></p>-->
                    		<?php endforeach; ?>
                    	</select>
                	</div>
                	<p class="pc">&gt;</p>
                	<div class="">
                		<label>中カテゴリ</label>
                		<select class="children" name="middle_category_id"  required="required" disabled>
                        <option value="" selected="selected">選択してください</option>
                		<?php foreach ($contents as $large_category_id => $content): ?>
                		<?php foreach ($content->middle_categories as $middle_category_id => $content_m): ?>
                		  <?= '<option value="' . substr($middle_category_id,1) . '"' . 'data-val="' . substr($large_category_id,1) . '">'; ?>
                		  <?= $content_m->name . '</option>'; ?>
                		<?php endforeach; ?>
                		<?php endforeach; ?>
                		</select>
                	</div>
                	<p class="pc">&gt;</p>
                	<div class="">
                		<label>小カテゴリ</label>
                		<select class="grandchildren" name="small_category_id" required="required" disabled>
                        <option value="" selected="selected">選択してください</option>
                		<?php foreach ($contents as $content): ?>
                		<?php foreach ($content->middle_categories as $middle_category_id => $content_m): ?>
                		<?php foreach ($content_m->small_categories as $small_category_id => $content_s): ?>
                		  <?= '<option value="' . substr($small_category_id,1) . '"' . 'data-valgc="' . substr($middle_category_id,1) . '">'; ?>
                		  <?= $content_s->name . '</option>'; ?>
                		<?php endforeach; ?>
                		<?php endforeach; ?>
                		<?php endforeach; ?>
                		</select>
                    </div>
                </div>
                        
            	
            	<!--<select class="parent" name="foo">-->
             <!--     <option value="" selected="selected">地方を選択</option>-->
             <!--     <option value="北海道・東北">北海道・東北</option>-->
             <!--     <option value="関東">関東</option>-->
             <!--     <option value="甲信越・北陸">甲信越・北陸</option>-->
             <!--     <option value="東海">東海</option>-->
             <!--     <option value="関西">関西</option>-->
             <!--     <option value="中国">中国</option>-->
             <!--     <option value="四国">四国</option>-->
             <!--     <option value="九州・沖縄">九州・沖縄</option>-->
             <!--   </select>-->
             <!--   <select class="children" name="bar" disabled>-->
             <!--     <option value="" selected="selected">都道府県を選択</option>-->
             <!--     <option value="北海道" data-val="北海道・東北">北海道</option>-->
             <!--     <option value="青森県" data-val="北海道・東北">青森県</option>-->
             <!--     <option value="岩手県" data-val="北海道・東北">岩手県</option>-->
             <!--     <option value="宮城県" data-val="北海道・東北">宮城県</option>-->
             <!--     <option value="秋田県" data-val="北海道・東北">秋田県</option>-->
             <!--     <option value="山形県" data-val="北海道・東北">山形県</option>-->
             <!--     <option value="福島県" data-val="北海道・東北">福島県</option>-->
             <!--     <option value="茨城県" data-val="関東">茨城県</option>-->
             <!--     <option value="栃木県" data-val="関東">栃木県</option>-->
             <!--     <option value="群馬県" data-val="関東">群馬県</option>-->
             <!--     <option value="埼玉県" data-val="関東">埼玉県</option>-->
             <!--     <option value="千葉県" data-val="関東">千葉県</option>-->
             <!--     <option value="東京都" data-val="関東">東京都</option>-->
             <!--     <option value="神奈川県" data-val="関東">神奈川県</option>-->
             <!--     <option value="新潟県" data-val="甲信越・北陸">新潟県</option>-->
             <!--     <option value="富山県" data-val="甲信越・北陸">富山県</option>-->
             <!--     <option value="石川県" data-val="甲信越・北陸">石川県</option>-->
             <!--     <option value="福井県" data-val="甲信越・北陸">福井県</option>-->
             <!--     <option value="山梨県" data-val="甲信越・北陸">山梨県</option>-->
             <!--     <option value="長野県" data-val="甲信越・北陸">長野県</option>-->
             <!--     <option value="岐阜県" data-val="東海">岐阜県</option>-->
             <!--     <option value="静岡県" data-val="東海">静岡県</option>-->
             <!--     <option value="愛知県" data-val="東海">愛知県</option>-->
             <!--     <option value="三重県" data-val="東海">三重県</option>-->
             <!--     <option value="滋賀県" data-val="関西">滋賀県</option>-->
             <!--     <option value="京都府" data-val="関西">京都府</option>-->
             <!--     <option value="大阪府" data-val="関西">大阪府</option>-->
             <!--     <option value="兵庫県" data-val="関西">兵庫県</option>-->
             <!--     <option value="奈良県" data-val="関西">奈良県</option>-->
             <!--     <option value="和歌山県" data-val="関西">和歌山県</option>-->
             <!--     <option value="鳥取県" data-val="中国">鳥取県</option>-->
             <!--     <option value="島根県" data-val="中国">島根県</option>-->
             <!--     <option value="岡山県" data-val="中国">岡山県</option>-->
             <!--     <option value="広島県" data-val="中国">広島県</option>-->
             <!--     <option value="山口県" data-val="中国">山口県</option>-->
             <!--     <option value="徳島県" data-val="四国">徳島県</option>-->
             <!--     <option value="香川県" data-val="四国">香川県</option>-->
             <!--     <option value="愛媛県" data-val="四国">愛媛県</option>-->
             <!--     <option value="高知県" data-val="四国">高知県</option>-->
             <!--     <option value="福岡県" data-val="九州・沖縄">福岡県</option>-->
             <!--     <option value="佐賀県" data-val="九州・沖縄">佐賀県</option>-->
             <!--     <option value="長崎県" data-val="九州・沖縄">長崎県</option>-->
             <!--     <option value="熊本県" data-val="九州・沖縄">熊本県</option>-->
             <!--     <option value="大分県" data-val="九州・沖縄">大分県</option>-->
             <!--     <option value="宮崎県" data-val="九州・沖縄">宮崎県</option>-->
             <!--     <option value="鹿児島県" data-val="九州・沖縄">鹿児島県</option>-->
             <!--     <option value="沖縄県" data-val="九州・沖縄">沖縄県</option>-->
             <!--   </select>-->
             <!--   <select class="grandchildren" name="barff" disabled>-->
             <!--     <option value="" selected="selected">数値を選択</option>-->
             <!--     <option value="500" data-valgc="福島県" >500</option>-->
             <!--     <option value="400" data-valgc="福島県" data-val="北海道・東北">400</option>-->
             <!--     <option value="300" data-valgc="青森県" >300</option>-->
             <!--     <option value="200" data-valgc="青森県" data-val="北海道・東北">200</option>-->
             <!--   </select>-->
            	
                <h2>入力した相談内容の確認</h2>
            	<!--<div class="question_form_box">-->
                <!--<form method="post" accept-charset="utf-8" action="/c9fdo/answers/add">-->
                <h3 class="category_select_heading">相談内容</h3>
                    <table class="check_contents_table">
                        <tbody>
                            <tr class="check_contents_row">
                        		<th>タイトル<span class="must">必須</span></th>
                        		<td class="check_contents">
                                    <div class="sample answer">
                                		<p><?= $_POST['title']; ?></p>
                                	</div>
                            	</td>
                            </tr>
                        	<tr class="check_contents_row">
                        		<th>相談内容<span class="must">必須</span></th>
                        		<td class="check_contents">
                                    <div class="sample answer">
                                		<p><?= $_POST['body']; ?></p>
                                	</div>
                            	</td>
                            </tr>
                        	<tr class="check_contents_row">
                        		<th>回答通知設定<span class="must">必須</span></th>
                        	    <td>
                        	        <?php if($_POST['reply_notice'] !== 0){
                            	        echo "<p>回答がついても通知をしない</p>";
                            	    }else{
                            	        echo "<p>回答がついたら通知をする</p>";
                            	    } ?>
                        	    </td>
                    	    </tr>
                	    </tbody>
                </table>
                <p class="text-center agree"><a href="/questions/under_construction">利用規約</a> 、<a href="/questions/under_construction">プライバシーポリシー</a> をよくお読みのうえ、「同意して送信する」のボタンをクリックしてください。</p>
                <div class="flex_center">
                	<input type="submit" name="back" value="戻る" class="btn_orange btn_color_gray margin_right10" formnovalidate="formnovalidate">
                	<input type="submit" name="submit" value="同意して送信する" class="btn_orange">
            	</div>
            	<input type="hidden" name="title" value="<?= $_POST['title']; ?>">
            	<input type="hidden" name="body" value="<?= $_POST['body']; ?>">
            	<input type="hidden" name="user_id" value="<?= $_POST['user_id']; ?>">
            	<input type="hidden" name="created" value="<?= $_POST['created']; ?>">
            	<input type="hidden" name="reply_notice" value="<?= $_POST['reply_notice']; ?>">
            </form>
            </div>

            
            <?php elseif ($page_flag === 2) : ?>
                <ol class="stepbar step3">
                    <li class="step">①相談内容の入力</li>
                    <li class="step">②カテゴリの選択・相談内容の確認</li>
                    <li class="step current">③相談完了</li>
                </ol>
                <?php if(isset($error_flag)){echo'
                    <h2>相談投稿が失敗しました！入力文字数を確認して再度投稿してください。</h2>
                    <div class="flex_center margin_v">
                    	<input type="button" onClick="location.href=\'/c9fdo/users/mypage\'" value="マイページに戻る" class="btn_orange btn_color_gray margin_right10">
                    	<input type="button" onClick="location.href=\'/c9fdo/questions/add\'" value="再度相談する" class="btn_orange">
                	</div>
                ';}else{echo'
                <h2>' .  $current_user["nickname"]  . 'さんの相談投稿が完了しました！</h2>
                <div class="post_complete_notes">
                    <h3><i class="fa fa-check-circle" aria-hidden="true"></i>ご投稿の相談に関する注意書き</h3>
                    <ul>
                        <li>
                            <p>ご投稿の相談は専門家からの回答がついて、運営側での確認が完了次第サイト上に公開されます。それまでは' .  $current_user["nickname"]  . 'さんと専門家以外の<span class="bold">一般ユーザーには公開がされません。</span></p>
                            <p>※サイト上の検索結果などには表示されません。</p>
                        </li>
                        <li>
                            <p><span class="bold">回答の受付期間は、投稿後最大14日間</span>となりますので、投稿した相談への追加投稿は期限内にお願いいたします。</p>
                            <p>※受付期間が終了しても、相談とその回答を閲覧することはできます。</p>
                        </li>
                        <li>
                            <p>直前の相談を終了しなければ、次回の投稿はできませんのでご注意ください。</p>
                            <p>※専門家から回答がつく、または投稿から24時間経過しなければ相談を終了させることはできません。</p>
                        </li>
                        <li>
                            <p><span class="bold">すべての質問に専門家から回答がつくとは限りません</span>ので、何卒ご理解いただきますようお願いいたします。</p>
                        </li>
                    </ul>
                </div>
                <div class="flex_center margin_v">
                	<input type="button" onClick="location.href=\'/c9fdo/users/mypage\'" value="マイページに戻る" class="btn_orange btn_color_gray margin_right10">
                	<input type="button" onClick="location.href=\'/c9fdo/questions/view/' . $question['id'] . '\'" value="投稿した記事へ" class="btn_orange">
            	</div>
            	';}?>
            <?php else: ?>
            
            <ol class="stepbar step3">
                <li class="step current">①相談内容の入力</li>
                <li class="step">②カテゴリの選択・相談内容の確認</li>
                <li class="step">③相談完了</li>
            </ol>
            <h2>相談内容の入力</h2>
            <div class="question_form_box">
                <div class="clearfix warning">
                    <p class="left"><i class="fa fa-check-circle" aria-hidden="true"></i>相談投稿における注意</p>
                    <p class="left">ご投稿後の変更・削除は原則行なっておりません。
                        <br>相談内容は一般に公開されるため、個人を特定されることの無いよう入力内容は十分ご注意ください。</p>
                </div>
                <form method="post" accept-charset="utf-8" action="/questions/add">
                <!--<?//= $this->Form->create($question,['type' => 'post','url' => ['controller' => 'Questions', 'action' => 'add']]) ?>-->
                    <input type="hidden" name="user_id" value="<?= $current_user["id"] ?>">
                    <input type="hidden" name="created" value="<?= date("Y/m/d H:i:s") ?>">
                    <table>
                        <tbody>
                            <tr>
                                <th>タイトル<span class="must">必須</span></th>
                                <td class="q_tit">
                                    <!--<?//= $this->Form->control('title',['type'=>'text']) ?>-->
                                    <input type="text" name="title" required="required" value="<?php if( !empty($_POST['title']) ){ echo $_POST['title']; } ?>">
                                    <p>入力できる文字数 : 残り <span class="bold count50"><?php if( !empty($_POST['title']) ){ echo 50 - mb_strlen($_POST['title']); }else{echo 50;} ?></span> 字(10文字以上入力してください)</p>
                                    <div class="sample">
                                        <p class="sample_tit bold">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>回答がつきやすいタイトル
                                        </p>
                                        <p>各分野の専門家や不動産業者が「自分の専門領域」と認識出来るタイトルを付けるようにお願い致します。
個人情報が特定されない範囲で、地名や学校名等の出来る限り詳細で具体的な情報を書き込めば、更に、的確な回答を得られます。
また、賃貸・売買・ローン・保険・管理・リフォーム・競売・不動産トラブル、と言ったように、カテゴリーをタイトルに入れると回答しやすいです。
</p>
                                        <div class="example">
                                            <p class="example_tit"><i class="fa fa-thumbs-up"></i>良いタイトルの例1</p>
                                            <p>学生生活＆バイト＆youtuber活動を両立させるのに適した賃貸物件は、どの駅が良いですか？</p>
                                            <p class="example_tit"><i class="fa fa-thumbs-down"></i>悪いタイトルの例1/p>
                                            <p>学生ですが、色々やりたいことがあるのですが、どうすれば良いですか？</p>
                                        </div>
                                        <div class="example">
                                            <p class="example_tit"><i class="fa fa-thumbs-up"></i>良いタイトルの例2</p>
                                            <p>隣地との境界確定で、不動産トラブルが起きており、隣人と揉めています。良い解決策はありますか？</p>
                                            <p class="example_tit"><i class="fa fa-thumbs-down"></i>悪いタイトルの例2</p>
                                            <p>隣人と揉めており、大ゲンカになって居ます。良い解決策はありますか？</p>
                                        </div>
                                        <div class="example">
                                            <p class="example_tit"><i class="fa fa-thumbs-up"></i>良いタイトルの例3</p>
                                            <p>新宿区50㎡2LDK築20年のマンションを購入予定ですが、ローン期間は、最長で何年付きますか？</p>
                                            <p class="example_tit"><i class="fa fa-thumbs-down"></i>悪いタイトルの例3</p>
                                            <p>マンションのローンについて困っています。話を聞いて下さい。</p>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>相談内容<span class="must">必須</span></th>
                                <td class="q_tit">
                                    <textarea name="body" required="required" ><?php if( !empty($_POST['body']) ){ echo $_POST['body']; } ?></textarea>
                                    <p>入力できる文字数 : 残り <span class="bold count"><?php if( !empty($_POST['body']) ){ echo 1000 - mb_strlen($_POST['title']); }else{echo 1000;} ?></span> 字(30文字以上入力してください)</p>
                                    <div class="sample">
                                        <p class="sample_tit bold">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i>回答がつきやすい投稿
                                        </p>
                                        <p>個人情報が特定されない範囲で、地名や学校名等の出来る限り詳細で具体的な情報を書き込めば、更に、的確な回答を得られます。
添付ファイルにて、動画や画像の添付も可能です。
物件の画像や、個人情報を消した上で、謄本公図地積測量図を添付も可能です。
カテゴリー・地域・性別・目的・費用金額、等のデータを出来る限り盛り込む事によって、専門家は詳細な回答がし易くなります。
</p>
                                        <div class="example">
                                            <p class="example_tit"><i class="fa fa-thumbs-up"></i>良い投稿の例</p>
                                            <p>Q.父親が初期ガンの診断を受けました。父親は不動産を所有していますが、相続対策はどうすれば良いですか？<br>
                                            父親は、東京都品川区内に①166.33㎡②141.61㎡、合計307.94㎡の土地（現状：駐車場）を所有しており、このままでは多額の相続税が掛かります。
良い相続対策を教えて下さいませ。
現在、借家住まいです。
家族構成は、父母と子供２人の計４人です。
参考に、土地地積測量図を添付致します。
</p>
                                            <p class="example_tit"><i class="fa fa-thumbs-down"></i>悪い投稿の例</p>
                                            <p>Q.父親が初期ガンの診断を受けました。父親は不動産を所有していますが、相続対策はどうすれば良いですか？<br>
                                            父親は、東京都内に駐車場を所有しており、このままでは多額の相続税が掛かります。
良い相続対策を教えて下さい。<br>
「駄目な理由」
→品川区まで書いて下さらないと、路線価は品川区の数値を基に計算するので近隣の専門家が対応しにくい。
→土地の面積㎡まで書いて下さらないと路線価の計算が出来ない。
→家族構成（父母と子供２人の計４人）を書いて下さらないと、相続税の計算が出来ない。
→添付ファイルで、地積測量図が貼って居ないと、どの程度の建物が建てられるか回答が出来ない。
</p>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>添付ファイル</th>
                                <td>
                                    <label for="file" class="file">
                                        <img src="/c9fdo/img/under/form_file.png" alt="添付ファイルを選択">
                                        <input type="file" id="file" />
                                    </label>
                                </td>
                            </tr>
                         <!--   <tr>
                                <th>回答通知設定</th>
                                <td class="q_tit">
                                    <?php
                                    if( !empty($_POST['reply_notice']) && $_POST['reply_notice'] === 0){
                                            echo'
                                            <input type="radio" id="radio01" name="reply_notice" value="ture">
                                            <label for="radio01">回答がついたら通知をする</label>
                                            <input type="radio" id="radio02" name="reply_notice" value="0" checked="checked">
                                            <label for="radio02">通知をしない</label>
                                            ';
                                    }else{
                                        echo'
                                        <input type="radio" id="radio01" name="reply_notice" value="ture" checked="checked">
                                        <label for="radio01">回答がついたら通知をする</label>
                                        <input type="radio" id="radio02" name="reply_notice" value="0">
                                        <label for="radio02">通知をしない</label>
                                        ';    
                                    } ?>
                                    
                                    <p>※通知メールは会員登録のメールアドレスに送信されます。</p>
                                </td>
                            </tr>-->
                        </tbody>
                    </table>
                    <p class="text-center agree"><a href="/questions/under_construction">利用規約</a> 、<a href="/questions/under_construction">プライバシーポリシー</a> をよくお読みのうえ、「同意して確認画面に進む」のボタンをクリックしてください。</p>
                    <button type="submit" class="btn_orange center" name="confirm" value="同意して確認画面に進む">同意して確認画面に進む</button>
                <!--</form>-->
                <?= $this->Form->end() ?>
                <?php endif; ?>
            </div>
        </article>
    </main>