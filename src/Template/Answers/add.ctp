<?php
//var_dump($_POST);
$page_flag = 0;
if( !empty($_POST['confirm']) ) {
	$page_flag = 1;
}elseif (!empty($_POST['submit'])) {
    $page_flag = 2; //完了画面
} else {
    $page_flag = 0; //入力画面
}
?>

<main class="inner">
        <ul class="bread">
            <li><a href="/c9fdo/index">不動産ドットコムTOP</a></li>
            <!--<li><a href="/c9fdo/questions/under_construction">不動産相続</a></li>-->
            <li>回答の投稿</li>
        </ul>
        <article>
            <!--<div class="estimate_ban02 pc">-->
                <!--<a href="/c9fdo/questions/under_construction" class="btn_white bold">一括見積について詳しく見る</a>-->
            <!--</div>-->
            <!-- /estimate_ban -->
            <div class="sp_estimate_ban02 sp">
                <a href="/c9fdo/questions/under_construction"><img src="/c9fdo/img/under/sp_estimate_ban02.png" alt="簡単入力で完全無料！複数の不動産の専門家に一括見積請求" class="img_responsive"></a>
            </div>
            <!-- /estimate_ban -->
            <p class="copy bold"><span>みんなの不動産相談</span>｜無料で優良不動産業者・専門家が回答する日本最大級の「不動産に関するQ＆Aサイト」</p>
            
            <?php if( $page_flag === 1 ): ?>
<!--<?//= debug($_POST) ?>-->
            <ol class="stepbar step3">
                <li class="step">①回答内容の入力</li>
                <li class="step current">②回答内容の確認</li>
                <li class="step">③回答完了</li>
            </ol>
            <h2>入力した回答内容の確認</h2>
            <div class="question_form_box">
            <form method="post" accept-charset="utf-8" action="/c9fdo/answers/add">
                <table>
                    <tbody>
                        <tr>
                            <th>回答対象の質問</th>
                            <td class="q_tit">
                                <div class="sample answer">
                        	    <p class="sample_tit bold">
                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i><?= $_POST["question_title"] ?>
                                </p>
                                <p><?= $_POST["question_body"] ?></p>
                        	    </div>
                        	</td>
                        </tr>
                        <tr>
                            <th>回答内容</th>
                            <td class="q_tit">
                                <div class="sample answer">
                        	    <p><?= $_POST['body']; ?></p>
                        	    </div>
                        	</td>
                        </tr>
                    </tbody>
                </table>
                <p class="text-center agree"><a href="/c9fdo/questions/under_construction">利用規約</a> 、<a href="/c9fdo/questions/under_construction">プライバシーポリシー</a> をよくお読みのうえ、「同意して送信する」のボタンをクリックしてください。</p>
                <div class="flex_center">
                	<input type="submit" name="back" value="戻る" class="btn_orange btn_color_gray margin_right10">
                	<input type="submit" name="submit" value="同意して送信する" class="btn_orange">
            	</div>
            	<input type="hidden" name="body" value="<?= $_POST['body']; ?>">
            	<input type="hidden" name="expert_id" value="<?= $_POST['expert_id']; ?>">
            	<input type="hidden" name="created" value="<?= $_POST['created']; ?>">
            	<input type="hidden" name="question_id" value="<?= $_POST['question_id']; ?>">
            	<input type="hidden" name="question_title" value="<?= $_POST['question_title']; ?>">
            	<input type="hidden" name="question_body" value="<?= $_POST['question_body']; ?>">
            	
            </form>
            </div>
            
            <?php elseif ($page_flag === 2) : ?>
                <ol class="stepbar step3">
                    <li class="step">①回答内容の入力</li>
                    <li class="step">②回答内容の確認</li>
                    <li class="step current">③回答完了</li>
                </ol>
                <?php if(isset($error_flag)){echo'
                    <h2>回答投稿が失敗しました！入力文字数を確認して再度投稿してください。</h2>
                    <div class="flex_center margin_v">
                    	<input type="button" onClick="location.href=\'/c9fdo/users/mypage\'" value="マイページに戻る" class="btn_orange btn_color_gray margin_right10">
                    	<input type="button" onClick="location.href=\'/c9fdo/questions/add\'" value="再度回答する" class="btn_orange">
                	</div>
                ';}else{echo'
                <h2>回答投稿が完了しました！</h2>
                <div class="flex_center margin_v">
                	<input type="button" onClick="location.href=\'/c9fdo/users/mypage\'" value="マイページに戻る" class="btn_orange btn_color_gray margin_right10">
                	<input type="button" onClick="location.href=\'/c9fdo/questions/view/' . $answer['question_id'] . '\'" value="回答した記事へ" class="btn_orange">
            	</div>
            	';}?>
            <?php else: ?>
            
            <ol class="stepbar step3">
                <li class="step current">①回答内容の入力</li>
                <li class="step">②回答内容の確認</li>
                <li class="step">③回答完了</li>
            </ol>
            <h2>回答内容の入力</h2>
            <div class="question_form_box">
                <div class="clearfix warning">
                    <p class="left"><i class="fa fa-check-circle" aria-hidden="true"></i>回答投稿における注意</p>
                    <p class="left">ご投稿後の変更・削除は原則行なっておりません。
                        <br>回答内容は一般に公開されるため、個人を特定されることの無いよう入力内容は十分ご注意ください。</p>
                </div>
                <form method="post" accept-charset="utf-8" action="/c9fdo/answers/add">
                    <input type="hidden" name="expert_id" value="<?= $current_user["id"] ?>">
                    <input type="hidden" name="created" value="<?= date("Y/m/d H:i:s") ?>">
                    <input type="hidden" name="question_id" value="<?php if(!empty($_POST['question_id'])){echo $_POST['question_id'];}else{echo $result[0]['id'];} ?>">
                    <input type="hidden" name="question_title" value="<?php if(!empty($_POST['question_title'])){echo $_POST['question_title'];}else{echo $result[0]["title"];} ?>">
                    <input type="hidden" name="question_body" value="<?php if(!empty($_POST['question_body'])){echo $_POST['question_body'];}else{echo $result[0]["body"];} ?>">
                    <table>
                        <tbody>
                            <tr>
                                <th>回答対象の質問<!--<span class="must"></span>--></th>
                                <td class="q_tit">
                                    <!--<input type="text" name="title" required="required" value="<?php// if( !empty($_POST['title']) ){ echo $_POST['title']; } ?>">-->
                                    <!--<p>入力できる文字数 : 残り <span class="bold">50</span> 字(10文字以上入力してください)</p>-->
                                    <div class="sample answer">
                                        <p class="sample_tit bold">
                                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i><?php if(!empty($_POST['question_title'])){echo $_POST['question_title'];}else{echo $result[0]["title"];} ?>
                                        </p>
                                        <p><?php if(!empty($_POST['question_body'])){echo $_POST['question_body'];}else{echo $result[0]["body"];} ?></p>
                                        <!--<div class="example">-->
                                        <!--    <p class="example_tit"><i class="fa fa-thumbs-up"></i>良いタイトルの例</p>-->
                                        <!--    <p>テキストテキストテキストテキストテキストテキストテキスト</p>-->
                                        <!--    <p class="example_tit"><i class="fa fa-thumbs-down"></i>悪いタイトルの例</p>-->
                                        <!--    <p>テキストテキストテキストテキストテキストテキストテキスト</p>-->
                                        <!--</div>-->
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>回答内容<span class="must">必須</span></th>
                                <td class="q_tit">
                                    <textarea name="body" required="required" ><?php if( !empty($_POST['body']) ){ echo $_POST['body']; } ?></textarea>
                                    <!--<textarea name="body" required="required" ><?php// if( !empty($_POST['body']) ){ echo '<%=' . $_POST['body'] . '%>'; } ?></textarea>-->
                                    <!--<input type="text" rows="4" cols="40" name="body" required="required" value="<?php// if( !empty($_POST['body']) ){ echo $_POST['body']; } ?>"></input>-->
                                    <p>入力できる文字数 : 残り <span class="bold count"><?php if( !empty($_POST['body']) ){ echo 1000 - mb_strlen($_POST['body']); }else{echo 1000;} ?></span> 字(30文字以上入力してください)</p>
                                    <!--<div class="sample">-->
                                    <!--    <p class="sample_tit bold">-->
                                    <!--        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>回答がつきやすい投稿-->
                                    <!--    </p>-->
                                    <!--    <p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>-->
                                    <!--    <div class="example">-->
                                    <!--        <p class="example_tit"><i class="fa fa-thumbs-up"></i>良い投稿の例</p>-->
                                    <!--        <p>テキストテキストテキストテキストテキストテキストテキスト</p>-->
                                    <!--        <p class="example_tit"><i class="fa fa-thumbs-down"></i>悪い投稿の例</p>-->
                                    <!--        <p>テキストテキストテキストテキストテキストテキストテキスト</p>-->
                                    <!--    </div>-->
                                    <!--</div>-->
                                </td>
                            </tr>
                            <!--<tr>-->
                            <!--    <th>添付ファイル</th>-->
                            <!--    <td>-->
                            <!--        <label for="file" class="file">-->
                            <!--            <img src="/c9fdo/img/under/form_file.png" alt="添付ファイルを選択">-->
                            <!--            <input type="file" id="file" />-->
                            <!--        </label>-->
                            <!--    </td>-->
                            <!--</tr>-->
                            <!--<tr>-->
                            <!--    <th>回答通知設定</th>-->
                            <!--    <td class="q_tit">-->
                            <!--        <?php/*-->
                            <!--        if( !empty($_POST['reply_notice']) && $_POST['reply_notice'] === 0){-->
                            <!--                echo'-->
                            <!--                <input type="radio" id="radio01" name="reply_notice" value="ture">-->
                            <!--                <label for="radio01">回答がついたら通知をする</label>-->
                            <!--                <input type="radio" id="radio02" name="reply_notice" value="0" checked="checked">-->
                            <!--                <label for="radio02">通知をしない</label>-->
                            <!--                ';-->
                            <!--        }else{-->
                            <!--            echo'-->
                            <!--            <input type="radio" id="radio01" name="reply_notice" value="ture" checked="checked">-->
                            <!--            <label for="radio01">回答がついたら通知をする</label>-->
                            <!--            <input type="radio" id="radio02" name="reply_notice" value="0">-->
                            <!--            <label for="radio02">通知をしない</label>-->
                            <!--            ';    -->
                            <!--        } */?>-->
                                    
                            <!--        <p>※通知メールは会員登録のメールアドレスに送信されます。</p>-->
                            <!--    </td>-->
                            <!--</tr>-->
                        </tbody>
                    </table>
                    <p class="text-center agree"><a href="/c9fdo/questions/under_construction">利用規約</a> 、<a href="/c9fdo/questions/under_construction">プライバシーポリシー</a> をよくお読みのうえ、「確認画面に進む」のボタンをクリックしてください。</p>
                    <button type="submit" class="btn_orange center" name="confirm" value="同意して確認画面に進む">同意して確認画面に進む</button>
                </form>
                <?php endif; ?>
            </div>
        </article>
    </main>