    <aside class="right">
      <!--<div class="number clearfix">-->
      <!--  <div class="left text-center member_num">-->
      <!--    <p class="num_tit"><i class="fa fa-vcard-o" aria-hidden="true"></i>専門家会員数</p>-->
      <!--    <p class="bold">1,329<span>名</span></p>-->
          <!--<p class="bold"><?//= $num_ex ?><span>名</span></p>-->
      <!--  </div>-->
      <!--  <div class="right text-center consultation_num">-->
      <!--    <p class="num_tit"><i class="fa fa-home" aria-hidden="true"></i>不動産相談件数</p>-->
      <!--    <p class="bold">123,456<span>件</span></p>-->
      <!--  </div>-->
      <!--</div>-->
      
      <div class="side_consultation">
        <h3 class="black">条件を変えて検索する</h3>
        <form>
          <input type="text" name="" class="prefectures"/>
          <input type="submit" value="検索" class="btn_green small"/>
        </form>
      </div>
      
      <div class="side_consultation">
        <h3 class="black">近くの専門家を探す</h3>
        <form>
          <fieldset>
          <!--<input type="text" name=""/>-->
          <?php echo $this->Form->select('prefecture_id',$prefectures,['class'=>'prefectures']); ?>
          
          <input type="submit" value="検索" class="btn_green small"/>
          </fieldset>
        </form>
      </div>
      
      <!-- /.number -->

      <div class="side_consultation">
        <h3 class="black">不動産の専門家に見積り依頼をする</h3>
        <a href="/c9fdo/questions/under_construction" class="btn_special orange"><img src="">今すぐ簡単一括見積</a>
      </div>
      <!-- /.side_consultation -->

      <div class="side_news">
        <h3 class="black">新しく相談する</h3>
        <a href="/c9fdo/questions/add" class="btn_special green"><img src="">今すぐプロに無料相談</a>
      </div>
      <!-- /side_news -->
      
      
      <div class="side_ranking">
        <h3 class="black">関連カテゴリから探す</h3>
        <div class="category_search_l"><i class="fa fa-check-circle" aria-hidden="true"></i>
        <?php echo $this->Html->link($large_categorie_list[$large_num], ['controller' => 'Articles', 'action' => 'categorieindex', $large_num + 1]) ?></div>
        <ul class="category_search_mid">
          
          <!--<?php// foreach ($choose_articles as $choose_article): ?>-->
          <?php foreach ($thearticles as $thearticle): ?>
          <li>
            <i class="fa fa-angle-right" aria-hidden="true"></i>
            <?php echo $this->Html->link($thearticle->name,['controller' => 'Articles', 'action' => 'categorieindex', "$thearticle->large_categorie_id","$thearticle->id"]) ?>
            <!--<?//= print_r(count($thearticles)) ?>-->
            <!--<?//= h($thearticle->name) ?>-->
          </li>
          <?php endforeach; ?>
        </ul>
      </div>
      <!-- /side_ranking -->

    </aside>
  </div>

