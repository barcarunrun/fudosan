<div class="search_category">
        <div class="inner">
            <ul class="bread">
                <li><a href="/c9fdo/index">不動産ドットコムTOP</a></li>
                <li><a href="/c9fdo/questions/inventory_l/<?= $category_l . '">' . $current_category->parent->name ?></a></li>
                <li><?= $current_category->name ?></li>
            </ul>
            
            <h1><img src="/c9fdo/img/top/<?= $current_category->parent->icon ?>" alt="" class="added_icon"><?= $current_category->name ?></h1>
            <div class="clearfix">
                <div class="left">
                    <img src="/c9fdo/img/under/<?= $current_category->img ?>" alt="" class="img_responsive">
                </div>
                <p class="right"><?= $current_category->description ?></p>
            </div>
        </div>
    </div>