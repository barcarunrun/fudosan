        <aside class="right">
            <div class="side_changing_conditions">
                <h3>条件を変えて検索する</h3>
                <div>
                    <?= $this->Form->create($keywords,['class'=>'clearfix']) ?>
                    <?= $this->Form->control('title',['type'=>'text','placeholder'=>'キーワードを入力して下さい。','class'=>'left']) ?>
                    <?= '<button type="submit" name="" class="right">検索</button>' ?>
                    <!--<?//= $this->Form->button('ログイン',['class'=>'btn_green small width02']) ?>-->
                    <?= $this->Form->end() ?>
                    <!--<form action="" method="get" class="clearfix">-->
                    <!--    <input type="text" name="" placeholder="あああ" class="left">-->
                    <!--    <button type="submit" name="" class="right">検索</button>-->
                    <!--</form>-->
                </div>
            </div>
            <div class="side_near_area">
                <h3>近くの専門家を探す</h3>
                <div>
                    <form action="" method="get" class="clearfix">
                        <div class="select_wrap left">
                            <select name="">
                                <option value="" selected>地域を選択して下さい</option>
                                <option value="北海道">北海道</option>
                                <option value="青森県">青森県</option>
                                <option value="岩手県">岩手県</option>
                                <option value="宮城県">宮城県</option>
                                <option value="秋田県">秋田県</option>
                                <option value="山形県">山形県</option>
                                <option value="福島県">福島県</option>
                                <option value="茨城県">茨城県</option>
                                <option value="栃木県">栃木県</option>
                                <option value="群馬県">群馬県</option>
                                <option value="埼玉県">埼玉県</option>
                                <option value="千葉県">千葉県</option>
                                <option value="東京都">東京都</option>
                                <option value="神奈川県">神奈川県</option>
                                <option value="新潟県">新潟県</option>
                                <option value="富山県">富山県</option>
                                <option value="石川県">石川県</option>
                                <option value="福井県">福井県</option>
                                <option value="山梨県">山梨県</option>
                                <option value="長野県">長野県</option>
                                <option value="岐阜県">岐阜県</option>
                                <option value="静岡県">静岡県</option>
                                <option value="愛知県">愛知県</option>
                                <option value="三重県">三重県</option>
                                <option value="滋賀県">滋賀県</option>
                                <option value="京都府">京都府</option>
                                <option value="大阪府">大阪府</option>
                                <option value="兵庫県">兵庫県</option>
                                <option value="奈良県">奈良県</option>
                                <option value="和歌山県">和歌山県</option>
                                <option value="鳥取県">鳥取県</option>
                                <option value="島根県">島根県</option>
                                <option value="岡山県">岡山県</option>
                                <option value="広島県">広島県</option>
                                <option value="山口県">山口県</option>
                                <option value="徳島県">徳島県</option>
                                <option value="香川県">香川県</option>
                                <option value="愛媛県">愛媛県</option>
                                <option value="高知県">高知県</option>
                                <option value="福岡県">福岡県</option>
                                <option value="佐賀県">佐賀県</option>
                                <option value="長崎県">長崎県</option>
                                <option value="熊本県">熊本県</option>
                                <option value="大分県">大分県</option>
                                <option value="宮崎県">宮崎県</option>
                                <option value="鹿児島県">鹿児島県</option>
                                <option value="沖縄県">沖縄県</option>
                            </select>
                        </div>
                        <button type="submit" name="" class="right">検索</button>
                    </form>
                </div>
            </div>
            <div class="side_estimate">
                <h3>不動産の専門家に見積もり依頼をする</h3>
                <div>
                    <a href="/c9fdo/questions/under_construction" class="btn_orange text-center"><img src="/c9fdo/img/under/estimate_icon.png" alt="">今すぐ簡単一括見積</a>
                </div>
            </div>
            <div class="side_new_consult">
                <h3>新しく相談する</h3>
                <div>
                    <a href="/c9fdo/questions/add" class="btn_green text-center"><img src="/c9fdo/img/under/side_q_icon.png" alt="">今すぐプロに無料相談</a>
                </div>
            </div>
            <div class="related_category">
                <h3>関連カテゴリから探す</h3>
                <div>
                    <ul>
                        <li class="current"><span><?= $current_category->parent->parent->name ?></span>
                            <ul>
                                <?php foreach ($current_category->parent->parent->middle_categories as $category_m => $content_m):?>
                                <li><a href="/c9fdo/questions/inventory_m/<?= $category_l . '/' . $category_m . '">' . '>' . $content_m->name ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </aside>
    </div>