
  <header>
    <div class="sp_utility sp">
        <div class="inner">
            <p>不動産ドットコム・不動産についての無料相談や、不動産専門家の検索</p>
        </div>
    </div>
    <div class="utility pc">
        <div class="clearfix inner">
            <p class="left">不動産ドットコム・不動産についての無料相談や、不動産専門家の検索</p>
            <ul class="right clearfix">
                <li class="left"><a href="/c9fdo/questions/add">不動産についての無料相談</a></li>
                <li class="left"><a href="/c9fdo/questions/under_construction">不動産ドットコムニュース</a></li>
                <li class="left"><a href="/c9fdo/users/login/expert">不動産業者&amp;専門家はこちら</a></li>
            </ul>
        </div>
    </div>
    
    <div class="sp_menu sp">
      <div class="inner clearfix">
        <h1><a href="/c9fdo/index" class="left"><img src="/c9fdo/img/header/logo.png" alt="不動産の相談なら不動産ドットコム"></a></h1>
        <div class="toggle">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <nav class="global_nav">
                    <ul>
                        <li><a href="/c9fdo/questions/add">不動産についての無料相談</a></li>
                        <li><a href="/c9fdo/questions/under_construction">不動産ドットコムニュース</a></li>
                        <li><a href="/c9fdo/users/login/expert">不動産業者&amp;専門家はこちら</a></li>
                        <li><a href="/c9fdo/users/login"><i class="fa fa-lock" aria-hidden="true"></i>ログイン</a></li>
                        <li><a href="/c9fdo/users/register"><i class="fa fa-plus" aria-hidden="true"></i>会員登録</a></li>
                        <li><a href="/c9fdo/questions/under_construction"><i class="fa fa-star" aria-hidden="true"></i>お気に入りの専門家<span>0</span></a></li>
                        <li><a href="/c9fdo/questions/under_construction"><i class="fa fa-question-circle" aria-hidden="true"></i>ヘルプ</a></li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="menu clearfix inner pc"><!-- img srcの呼び方 要改 -->
            <a href="/c9fdo/index" class="left"><img src="/c9fdo/img/header/logo.png" alt="不動産の相談なら不動産ドットコム"></a>
            <ul class="right clearfix">
                <!--<li class="left">-->
                <!--    <a href="/c9fdo/users/login">-->
                <!--        <div>-->
                <!--            <i class="fa fa-lock" aria-hidden="true"></i>-->
                <!--        </div>-->
                <!--        <span>ログイン</span>-->
                <!--    </a>-->
                <!--</li>-->
                <!--<li class="left">-->
                <!--    <a href="/c9fdo/users/add">-->
                <!--        <div>-->
                <!--            <i class="fa fa-plus" aria-hidden="true"></i>-->
                <!--        </div>-->
                <!--        <span>会員登録</span>-->
                <!--    </a>-->
                <!--</li>-->
                <?php
                  if(is_null($current_user)) {
                    echo '<li class="left">
                            <a href="/c9fdo/users/login">
                              <div>
                                <i class="fa fa-lock" aria-hidden="true"></i>
                              </div>
                              <span>ログイン</span>
                            </a>
                          </li>
                          <li class="left">
                            <a href="/c9fdo/users/register">
                              <div>
                                <i class="fa fa-plus" aria-hidden="true"></i>
                              </div>
                              <span>会員登録</span>
                            </a>
                          </li>';
                  } else {
                    echo '<li class="left">
                            <a href="/c9fdo/users/mypage/' . $current_user["id"] . '">
                              <div><i class="fa fa-user" aria-hidden="true"></i></div>
                              <span>マイページ</span>
                            </a>
                          </li>
                          <li class="left">
                            <div class="popupbtn">
                              <span class="trigger">
                                <div><i class="fa fa-cog" aria-hidden="true"></i></div>
                                <span>設定</span>
                              </span>
                              <div class="popup" ><!-- style="opacity: 0; display: none;"> -->
                                <ul class="submenu">
                                  <li><a class="uaLbl_341" href="/c9fdo/questions/under_construction">登録情報の変更</a></li>
                                  <li><a href="/c9fdo/questions/under_construction">パスワードの変更</a></li>
                                </ul>
                                <p class="logout"><a title="ログアウト" href="/c9fdo/users/logout/">ログアウト</a></p>
                              </div>
                            </div>
                          </li>';
                  };
                ?>
                <li class="left bookmark">
                    <a href="/c9fdo/questions/under_construction">
                        <div>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <p class="bookmark_count">0</p>
                        </div>
                        <span>お気に入りの専門家</span></a>
                </li>
                <li class="left">
                    <a href="/c9fdo/questions/under_construction">
                        <div>
                            <i class="fa fa-question-circle" aria-hidden="true"></i>
                        </div>
                        <span>ヘルプ</span>
                    </a>
                </li>
            </ul>
        </div>
    </header>
  <div class="top_info">
      <div class="inner">
          <div class="info_close">閉じる<i class="fa fa-close" aria-hidden="true"></i></div>
          <p class="bold">「不動産ドットコム」は日本最大級の、不動産「相談」ポータルサイトです</p>
          <p style="width:90%;">「不動産ドットコム」は、不動産の専門家に無料で相談できる「みんなの不動産相談」や、地域や分野などから、評判の良い不動産屋さんや、リフォーム業者・不動産関連の専門職士業事務所を探せる「不動産業界検索」など、不動産関連の相談や、不動産トラブルの解決サポートするコンテンツを多数ご用意しています。詳しくは「はじめての方へ」をご覧ください。</p>
      </div>
  </div>