  <footer>
    <div class="inner clearfix">
      <div class="left">
        <p>運営会社：株式会社 不動産ドットコム</p>
        <small>Copyright © 不動産ドットコム All rights reserved.</small>
      </div>
      <ul class="right clearfix">
        <li class="left"><a href="/c9fdo/questions/under_construction">運営会社情報</a></li>
        <li class="left"><a href="/c9fdo/questions/under_construction">利用規約</a></li>
        <li class="left"><a href="/c9fdo/questions/under_construction">プライバシーポリシー</a></li>
      </ul>
    </div>
  </footer>
  <script
  src="https://code.jquery.com/jquery-1.12.4.min.js"
  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
  crossorigin="anonymous"></script>

  <script src="/c9fdo/js/top.js" type="text/javascript" charset="utf-8" async defer></script>
  <!-- <script src="dest/js/top.js" type="text/javascript" charset="utf-8" async defer></script> -->
  
  <!--設定のマウスオーバー-->
  <!--後で不要なものけす-->
  <script type="text/javascript">
    $(function () {
      $(".left>.popupbtn").each(function () {
        var h = -10;
        var g = 150;
        var c = 250;
        var b = null;
        var e = false;
        var d = false;
        var a = $(".trigger", this);
        var f = $(".popup", this).css("opacity", 0);
        $([a.get(0), f.get(0)]).mouseover(function () {
          if (b) {
            clearTimeout(b)
          }
          if (e || d) {
            return
          } else {
            e = true;
            f.css({display: "block"}).animate({opacity: 1}, g, "swing", function () {
              e = false;
              d = true
            })
          }
          return false
        });
        $([a.get(0), f.get(0)]).mouseout(function () {
          if (b) {
            clearTimeout(b)
          }
          b = setTimeout(function () {
            b = null;
            f.animate({opacity: 0}, g, "swing", function () {
              d = false;
              f.css("display", "none")
            })
          }, c);
          return false
        })
      })
    });
  </script>
  
  <!--絞り込みselect-->
  <script type="text/javascript">
      var $children = $('.children'); //都道府県の要素を変数に入れます。
      var original = $children.html(); //後のイベントで、不要なoption要素を削除するため、オリジナルをとっておく
      
       
      //地方側のselect要素が変更になるとイベントが発生
      $('.parent').change(function() {
        //選択された地方のvalueを取得し変数に入れる
        var val1 = $(this).val();
        //削除された要素をもとに戻すため.html(original)を入れておく
        $children.html(original).find('option').each(function() {
          var val2 = $(this).data('val'); //data-valの値を取得
          //valueと異なるdata-valを持つ要素を削除
          if (val1 != val2) {
            $(this).not(':first-child').remove();
          }
        });
        
        //地方側のselect要素が未選択の場合、都道府県をdisabledにする
        if ($(this).val() == "") {
          $children.attr('disabled', 'disabled');
        } else {
          $children.removeAttr('disabled');
        }
        
      });
  </script>
  <script type="text/javascript">
      var $grandchildren = $('.grandchildren'); //都道府県の要素を変数に入れます。
      var grandoriginal = $grandchildren.html();
       
        //地方側のselect要素が変更になるとイベントが発生
        $('.children').change(function() {
          //選択された地方のvalueを取得し変数に入れる
          var val3 = $(this).val();
          //削除された要素をもとに戻すため.html(original)を入れておく
          $grandchildren.html(grandoriginal).find('option').each(function() {
            var val4 = $(this).data('valgc'); //data-valの値を取得
            //valueと異なるdata-valを持つ要素を削除
            if (val3 != val4) {
              $(this).not(':first-child').remove();
            }
         
          });
       
        
        if ($(this).data('val') == "") {
          $grandchildren.attr('disabled', 'disabled');
        } else {
          $grandchildren.removeAttr('disabled');
        }
       
      });
  </script>
  
  <!--フォーム文字数50カウントダウン-->
  <script type="text/javascript">
    $(function(){
      var countMax = 50;
      $('input').bind('keydown keyup keypress hcange',function(){
        var thisValueLength = $(this).val().length;
        var countDown = (countMax)-(thisValueLength);
        $('.count50').html(countDown);
        if(countDown < 0){
          $('.count50').css({color:'#ff0000'});
        }else{
          $('.count50').css({color:'#000000'});
        }
      })
    });
  </script>
  
  <!--フォーム文字数1000カウントダウン-->
  <script type="text/javascript">
    $(function(){
      var countMax = 1000;
      $('textarea').bind('keydown keyup keypress hcange',function(){
        var thisValueLength = $(this).val().length;
        var countDown = (countMax)-(thisValueLength);
        $('.count').html(countDown);
        if(countDown < 0){
          $('.count').css({color:'#ff0000'});
        }else{
          $('.count').css({color:'#000000'});
        }
      })
    });
  </script>
</body>
</html>
