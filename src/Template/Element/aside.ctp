    <aside class="right">
      <div class="number clearfix">
        <div class="left text-center member_num">
          <p class="num_tit"><i class="fa fa-vcard-o" aria-hidden="true"></i>専門家会員数</p>
          <p class="bold">1,329<span>名</span></p>
          <!--<p class="bold"><?//= $num_ex ?><span>名</span></p>-->
        </div>
        <div class="right text-center consultation_num">
          <p class="num_tit"><i class="fa fa-home" aria-hidden="true"></i>不動産相談件数</p>
          <p class="bold">123,456<span>件</span></p>
        </div>
      </div>
      <!-- /.number -->

      <div class="side_consultation">
        <h3>NEW!新着不動産相談</h3>
        <?php foreach ($results as $result):?>
        <!--<div class="consultation01">-->
        <div class="">
          <img src="img/top/category_icon<?= $result["large_category_id"] ?>.png" alt="" class="added_icon_aside">
          <p><a href="/c9fdo/questions/view/<?= $result["id"] . '">' .  $this->shorthand($result["title"],31); ?></a></p>
        </div>
        <?php endforeach; ?>
        <!--<div class="consultation01">-->
        <!--  <p><a href="/c9fdo/questions/under_construction"></a>事務所＆店舗物件の敷金負担を減らす方法を教えて？</a></p>-->
        <!--</div>-->
        <!--<div class="consultation01">-->
        <!--  <p><a href="/c9fdo/questions/under_construction"></a>オール電化と、ガス併用住宅のメリットデメリットについて</a></p>-->
        <!--</div>-->
        <!--<div class="consultation02">-->
        <!--  <p><a href="/c9fdo/questions/under_construction"></a>銀行に新築費用のローンを申込んだところ、断られてしまいました。どう…</a></p>-->
        <!--</div>-->
        <!--<div class="consultation01">-->
        <!--  <p><a href="/c9fdo/questions/under_construction"></a>事務所＆店舗物件の敷金負担を減らす方法を教えて？</a></p>-->
        <!--</div>-->
        <div class="text-center">
          <a href="questions/search" class="btn_border">＋ 続きをみる</a>
        </div>
      </div>
      <!-- /.side_consultation -->

      <div class="side_news">
        <h3>不動産ドットコムニュース</h3>
        <ul>
          <li><a href="/c9fdo/questions/under_construction">首都圏中古マンション価格、引き続き上昇</a></li>
          <li><a href="/c9fdo/questions/under_construction">未婚女性の住宅購入理由、トップは「家賃を払い続けるのがも…</a></li>
          <li><a href="/c9fdo/questions/under_construction">三鷹駅直結タワーマンション販売、相鉄不動産と三菱地所レジ…</a></li>
          <li><a href="/c9fdo/questions/under_construction">11月の首都圏マンション市場、新規発売戸数は3ヶ月ぶりに…</a></li>
        </ul>
        <div class="text-center">
          <a href="/c9fdo/questions/under_construction" class="btn_border">＋ 続きをみる</a>
        </div>
      </div>
      <!-- /side_news -->

      <div class="side_ranking">
        <h3>不動産専門家ランキング</h3>
        <a href="/c9fdo/questions/under_construction" class="rank_box">
          <div class="clearfix">
            <div class="left">
              <p>RANK.<span class="bold">1</span></p>
              <p class="bold">山本 太郎<span>東京都</span></p>
              <p>司法書士</p>
            </div>
            <img src="img/top/dummy_rank1.png" alt="" class="right">
          </div>
        </a>
        <a href="/c9fdo/questions/under_construction" class="rank_box">
          <div class="clearfix">
            <div class="left">
              <p>RANK.<span class="bold">2</span></p>
              <p class="bold">佐藤 愛<span>福岡県</span></p>
              <p>弁護士</p>
            </div>
            <img src="img/top/dummy_rank2.png" alt="" class="right">
          </div>
        </a>

        <a href="/c9fdo/questions/under_construction" class="rank_box">
          <div class="clearfix">
            <div class="left">
              <p>RANK.<span class="bold">3</span></p>
              <p class="bold">鈴木 健一郎<span>東京都</span></p>
              <p>宅地建物取引士</p>
            </div>
            <img src="img/top/dummy_rank3.png" alt="" class="right">
          </div>
        </a>

        <a href="/c9fdo/questions/under_construction" class="rank_box">
          <div class="clearfix">
            <div class="left">
              <p>RANK.<span class="bold">4</span></p>
              <p class="bold">田中 真司<span>石川県</span></p>
              <p>税理士</p>
            </div>
            <img src="img/top/dummy_rank4.png" alt="" class="right">
          </div>
        </a>

        <a href="/c9fdo/questions/under_construction" class="rank_box">
          <div class="clearfix">
            <div class="left">
              <p>RANK.<span class="bold">5</span></p>
              <p class="bold">佐々木 史郎<span>大阪府</span></p>
              <p>不動産鑑定士</p>
            </div>
            <img src="img/top/dummy_rank5.png" alt="" class="right">
          </div>
        </a>

      </div>
      <!-- /side_ranking -->

    </aside>
  </div>

