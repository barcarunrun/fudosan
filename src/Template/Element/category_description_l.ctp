<div class="search_category">
    <div class="inner">
        <ul class="bread">
            <li><a href="/c9fdo/index">不動産ドットコムTOP</a></li>
            <li><?= $current_category->name ?></li>
        </ul>
        <h1><img src="/c9fdo/img/top/<?= $current_category->icon ?>" alt="" class="added_icon"><?= $current_category->name ?></h1>
        <div class="clearfix" >
            <p class="left"><?= $current_category->description ?></p>
            <a href="/c9fdo/questions/under_construction"><img src="/c9fdo/img/under/large_category_search_ban.png" alt="詳しい専門家を探す" class="right"></a>
        </div>
        <div class="clearfix" style="column-count: 2;">
        
            <?php foreach ($current_category->middle_categories as $category_m => $content_m):?>
            <div style="display: inline-block;">
            <div class="medium_category_box" id="<?= $category_m ?>">
                <img src="/img/under/<?= $content_m->img ?>" alt="" class="img_responsive"style="height:275px;">
                <div class="bold"style="display:block;">
                    <!--<a href="/c9fdo/questions/inventory_m/<?= $category_l . '/' . $category_m ?>">-->
                        <p><?= $content_m->name ?></p>
                    <!--</a>-->
                    <p class="p_normal" ><?= $content_m->description ?></p>
                    <ul>
                        
                        <?php foreach ($content_m->small_categories as $category_s => $content_s):?>
                        <li style="line-height: 20px;">
                            <a href="/questions/inventory_s/<?= $category_l . '/' . $category_m . '/' . $category_s ?>" style="color: #005ebb!important;">
                                <i class="fa fa-arrow-right" aria-hidden="true"></i>
                                
                                <?= $content_s->name ?>
                                
                            </a>
                        </li>
                        <?php endforeach; ?>
                         
                    </ul>
                </div>
            </div>
            </div>
            <?php endforeach; ?>
            
        </div>
    </div>
</div>