<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\View\Helper\HtmlHelper;
use Cake\View\Helper\FormHelper;


/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{
    public $paginate = [
        'limit' => 5,
        'order' => [
            'Users.created' => 'desc'
        ]
    ];
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['logout', 'register']);
        
        //ログイン状態の確認用変数取得
        $current_user = $this->Auth->user();
        $this->set('current_user', $current_user);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $users = $this->paginate($this->Users);

        $this->set(compact('users'));
        $this->set('_serialize', ['users']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        // $user = $this->Users->get($id, [
        //     'contain' => ['Articles']
        // ]);

        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
     
    public function register()
    {
        $this->viewBuilder()->layout('register_u');
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));
                return $this->redirect($this->Auth->redirectUrl());
                // return $this->redirect(['controller'=>'tops','action' => 'index']);
            }
            $this->Flash->error(__('新規登録に失敗しました。最初からもう一度登録をお願いします。'));
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    
        //都道府県
        $prefectures = TableRegistry::get('prefectures')->find()->toArray();
        $this->set('prefectures',$prefectures);
    }
    public function registerE()
    {
        $this->viewBuilder()->layout('register_e');
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
        $this->set('_serialize', ['user']);
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    public function login($flag_expert=null)
    {
        $this->viewBuilder()->layout('login');
        
        $this->set(compact('flag_expert'));
        
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                // return $this->referer();
                // return $this->redirect($this->referer());
                // return $this->redirect($this->Auth->redirectUrl($returnpage));
                // return $this->redirect($this->Auth->redirectUrl($this->referer()));
                // return $this->redirect($this->Auth->redirectUrl('/index'));
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error('ユーザー名またはパスワードが不正です。');
        }
        // debug($_SERVER['HTTP_REFERER']);
        // debug($this->referer());
        // debug($this->Auth->redirectUrl());
        
        // $this->protectAuthRedirect('true');
    }
    public function logout()
    {
        // $this->Flash->success('ログアウトしました。');//あとで使うかも
        $this->Auth->logout();
        // return $this->redirect($this->Auth->redirectUrl($this->referer()));
        return $this->redirect($this->Auth->redirectUrl('/index'));
        // return $this->redirect($this->Auth->logout());
        
    }
    public function mypage($user_id=null)
    {
        $this->viewBuilder()->layout('mypage');
        
        $this->set(compact('user_id'));
        $this->set(compact('questions'));
    }
}
