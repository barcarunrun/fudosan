<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\File;

class TopsController extends AppController
{
	public function initialize()
	{
		parent::initialize();
		$this->viewBuilder()->layout('pattern01');
	}

	public function index()
	{
		//ログイン状態の確認用変数取得//どこかで一括で定義したい
        $current_user = $this->Auth->user();
        $this->set('current_user', $current_user);
        
        //ようこそ表示用名前取得
        if(isset($current_user)){
            $temp1 = $current_user['email'];
            $end1 = stripos($temp1,'@');
            $name1 = substr($temp1,0,$end1);
            $this->set('name1',$name1);
            // unset($temp1,$end1,$name1);
        };
        
        //専門家会員数取得用変数
        $temp = TableRegistry::get('experts');
        $query = $temp->find();
        $total = [];
        foreach ($query as $row):
            $total[] = $row->id;
        endforeach;
        $this->set('num_ex', count($total));
        unset($temp);
        
        $file = new File('categories.json');
        $contents = json_decode($file->read());
        // $this->set('content',rror());
        $this->set('contents',$contents);
	}

}