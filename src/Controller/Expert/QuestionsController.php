<?php
namespace App\Controller\Expert;

use App\Controller\Expert\AppController;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\File;
use Cake\Datasource\ConnectionManager;
use Cake\View\Helper\FormHelper;

class QuestionsController extends AppController
{
	public function initialize()
	{
		parent::initialize();
		
		//ログイン状態の確認用変数取得
        $current_user = $this->Auth->user();
        $this->set('current_user', $current_user);
        
        //カテゴリ一覧表示用
        //$file = new File('../src/Model/categories.json');
        $file = new File('/src/Model/categories.json');
        
        $contents = json_decode($file->read());
        $this->set('contents',$contents);
        $this->set('_serialize', ['contents']);
        
        
        
	}

	public function index()
	{
		$this->viewBuilder()->layout('toppage');
		//新着不動産相談
		$connection = ConnectionManager::get('c9fdo_db');
		$results = $connection->execute('SELECT * FROM questions ORDER BY created DESC LIMIT 0,4')->fetchAll('assoc');
		$this->set(compact('results'));
		
        //ようこそ表示用名前取得
        if(isset($current_user)){
            $temp1 = $current_user['email'];
            $end1 = stripos($temp1,'@');
            $name1 = substr($temp1,0,$end1);
            $this->set('name1',$name1);
            // unset($temp1,$end1,$name1);
        };
        
        //専門家会員数取得用変数
        $temp = TableRegistry::get('experts');
        $query = $temp->find();
        $total = [];
        foreach ($query as $row):
            $total[] = $row->id;
        endforeach;
        $this->set('num_ex', count($total));
        unset($temp);
        
        //キーワード検索用
        $keywords = $this->Questions->newEntity();
        $this->set('keywords',$keywords);
        $this->set('_serialize', ['keywords']);
        if ($this->request->is('post')) {
        	return $this->redirect(['action' => 'search']);
        };
        
	}
	public function inventoryL($category_l=null)
	{
		$this->viewBuilder()->layout('inventory_l');
		//引数をset
		$this->set(compact('category_l'));
		//現在の引数のカテゴリをset
		$current_category = $this->get_category($category_l);
		$this->set(compact('current_category'));
	}
	
	public function inventoryM($category_l=null,$category_m=null)
	{
		$this->viewBuilder()->layout('inventory_m');
		//引数をset
		$this->set(compact('category_l','category_m'));
		//現在の引数のカテゴリをset
		$current_category = $this->get_category($category_l,$category_m);
		$this->set(compact('current_category'));
	}
	public function inventoryS($category_l=null,$category_m=null,$category_s=null)
	{
		$this->viewBuilder()->layout('inventory_s');
		//引数をset
		$this->set(compact('category_l','category_m','category_s'));
		//現在の引数のカテゴリをset
		$current_category = $this->get_category($category_l,$category_m,$category_s);
		$this->set(compact('current_category'));
	}
	public function search()
	{
	    $this->viewBuilder()->layout('general');
	    
        //キーワード検索用
        $keywords = $this->Questions->newEntity();
        $this->set('keywords',$keywords);
        $this->set('_serialize', ['keywords']);
        if ($this->request->is('post')) {
        	return $this->redirect(['action' => 'search']);
        };
	}
	public function view($question_id=null)
	{
	    $this->viewBuilder()->layout('general');
	    // 仮に
	    $question_id = 7;
	    //指定質問の表示
	    $connection = ConnectionManager::get('c9fdo_db');
		$result = $connection->execute('SELECT * FROM questions WHERE id = :id', ['id' => $question_id])->fetch('assoc');
		$this->set(compact('result'));
		
	    $category_l = 'l' . $result['large_category_id'];
	    $category_m = 'm' . $result['middle_category_id'];
	    $category_s = 's' . $result['small_category_id'];
	    $current_category = $this->get_category($category_l,$category_m,$category_s);
		$this->set(compact('current_category','category_l','category_m','category_s'));
		
        //キーワード検索用
        $keywords = $this->Questions->newEntity();
        $this->set('keywords',$keywords);
        $this->set('_serialize', ['keywords']);
        if ($this->request->is('post')) {
        	return $this->redirect(['action' => 'search']);
        };
		
	}
	public function add()
	{
	    $this->viewBuilder()->layout('form');
	    // $questions = TableRegistry::get('questions');
	    $question = $this->Questions->newEntity();
	    // $question = $questions->newEntity();
        if ($this->request->is('post') && isset($_POST['submit'])) {
            $question = $this->Questions->patchEntity($question, $this->request->data);
            if ($this->Questions->save($question)) {
                // $this->Flash->success(__('The article has been saved.'));
                // return $this->redirect(['controller' => 'Users' , 'action' => 'mypage']);
            }
            $this->Flash->error(__('The article could not be saved. Please, try again.'));
        }
        // $users = $this->questions->Users->find('list', ['limit' => 200]);
        // $midCategories = $this->questions->MidCategories->find('list', ['limit' => 200]);
        // $this->set(compact('question', 'users', 'midCategories'));
        $this->set(compact('question'));
        $this->set('_serialize', ['question']);
        
        //リスト大カテゴリ
        $large_categories = TableRegistry::get('large_categories');
        $this->set('large_categories',$large_categories->find('list',array('fields'=>array('id','name'))));
        //リスト中カテゴリ
        $middle_categories = TableRegistry::get('middle_categories');
        $this->set('middle_categories',$middle_categories->find('list',array('fields'=>array('id','name'))));
        //リスト小カテゴリ
        $small_categories = TableRegistry::get('small_categories');
        $this->set('small_categories',$small_categories->find('list',array('fields'=>array('id','name'))));
	}

}