<?php
namespace App\Controller\Expert;
// namespace App\Controller;

use App\Controller\Expert\AppController;
use Cake\ORM\TableRegistry;
use Cake\View\Helper\HtmlHelper;
use Cake\View\Helper\FormHelper;


class UsersController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['logout', 'register']);
        
        //ログイン状態の確認用変数取得
        $current_user = $this->Auth->user();
        $this->set('current_user', $current_user);
    }
    public function register()
    {
        $this->viewBuilder()->layout('register_e');
    }
    public function login()
    {
        $this->viewBuilder()->layout('login');
        // $this->modelClass = TableRegistry::get('experts');
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                // return $this->redirect($this->Auth->redirectUrl($this->request->referer()));
                return $this->redirect($this->Auth->redirectUrl('/index'));
                
            }
            $this->Flash->error('ユーザー名またはパスワードが不正です。02');
        }
        $this->set(compact('user'));
        // $this->_setDefaults();
        $jjj=500;
        $this->set('jjj',$jjj);
        // debug($this->request->data);
        // debug($this->Auth->login());
        // debug($this->authenticate);
        // debug($this->modelClass);
        // debug($this->login());
    }
}