<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller\Expert;
// namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Filesystem\File;
use Cake\Datasource\ConnectionManager;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        
        

        /*
         * Enable the following components for recommended CakePHP security settings.
         * see http://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
        //$this->loadComponent('Csrf');
        // if ($this->request->prefix == 'expert') {
            $this->loadComponent('Auth', [
                'authenticate' => [
                    'Form' => [
                        'userModel' => 'Experts',
                        'fields' => [
                            'username' => 'email',
                            'password' => 'password'
                        ]
                    ]
                ],
                'loginAction' => [
                    'controller' => 'Users',
                    'action' => 'login'
                ],
                // 未認証の場合、直前のページに戻します
                'unauthorizedRedirect' => $this->referer()
            ]);
        // }else{
        //     $this->loadComponent('Auth', [
        //         'authenticate' => [
        //             'Form' => [
        //                 // 'userModel' => 'Users',
        //                 'fields' => [
        //                     'username' => 'email',
        //                     'password' => 'password'
        //                 ]
        //             ]
        //         ],
        //         'loginAction' => [
        //             'controller' => 'Users',
        //             'action' => 'login'
        //         ],
        //         // 未認証の場合、直前のページに戻します
        //         'unauthorizedRedirect' => $this->referer()
        //     ]);
        // }

        // display アクションを許可して、PagesController が引き続き
        // 動作するようにします。また、読み取り専用のアクションを有効にします。
        $this->Auth->allow(['display', 'view', 'index', 'categorieindex','search','inventoryL','inventoryM','inventoryS','login']);
        
        
        $dsn = 'mysql://root:@localhost/c9fdo_db';
		ConnectionManager::config('c9fdo_db', ['url' => $dsn]);
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return \Cake\Network\Response|null|void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
        // redirectUrlを随時更新
        $this->Auth->redirectUrl($this->referer());
    }
    
    function get_category($l=null,$m=null,$s=null)
    {
        //カテゴリ一覧表示用
        $file = new file('../src/Model/categories.json');
        $contents = json_decode($file->read());
        // $this->set('contents',$contents);
        // $this->set('_serialize', ['contents']);
        
        $lc = $contents->$l;
        if(is_null($m)){
            return $lc;
        }else{
            $mc = $lc->middle_categories->$m;
            $mc->parent = $lc;
            if(is_null($s)){
                return $mc;
            }else{
                $sc = $mc->small_categories->$s;
                $sc->parent = $mc;
                return $sc;
            }
        }
    }
    
}
