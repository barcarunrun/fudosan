<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\File;
use Cake\View\Helper\FormHelper;

class AnswersController extends AppController
{
	public function initialize()
	{
	    parent::initialize();
	    
	    //ログイン状態の確認用変数取得
        $current_user = $this->Auth->user();
        $this->set('current_user', $current_user);
	}
	
	public function add($question_id=null)
	{
	    $this->viewBuilder()->layout('form');
	    
	 //   $connection = ConnectionManager::get('c9fdo_db');
		// $result = $connection->execute('SELECT * FROM questions WHERE id = :id', ['id' => $question_id])->fetch('assoc');
		$questions = $this->Questions;
		$result = $questions
			->find()
			->where(['id'=>$question_id])
			->toArray();
		$this->set(compact('result'));
		$this->set('_serialize', ['result']);
		// debug($result);
	    
	    $answer = $this->Answers->newEntity();
        if ($this->request->is('post') && isset($_POST['submit'])) {
            $answer = $this->Answers->patchEntity($answer, $this->request->data);
            if ($this->Answers->save($answer)) {
                // $this->Flash->success(__('The article has been saved.'));
                // return $this->redirect(['controller' => 'Users' , 'action' => 'mypage']);
            }else{
            $this->Flash->error(__('投稿に失敗しました。文字数を確認して再度投稿してください。'));
            $error_flag = 1;
            $this->set(compact('error_flag'));
            }
        }
        $this->set(compact('answer'));
        $this->set('_serialize', ['answer']);
	}
	
}