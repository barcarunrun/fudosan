<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Filesystem\File;
// use Cake\Datasource\ConnectionManager;
// use Cake\View\Helper\HtmlHelper;
use Cake\View\Helper\FormHelper;
use Cake\Validation\Validator;

class QuestionsController extends AppController
{
	public $paginate = [
        'limit' => 5,
        'order' => [
            'questions.created' => 'desc'
        ]
    ];
    public $helpers = [
    'Paginator' => ['templates' => 
        'paginator-templates']
    ];
	
	public function initialize()
	{
		parent::initialize();
		// $this->loadComponent('Paginator');//なぜなくてもうごくのか？
		
		//ログイン状態の確認用変数取得
        $current_user = $this->Auth->user();
        $this->set('current_user', $current_user);
        
        //カテゴリ一覧表示用
        $file = new File('src/Model/categories.json');

        $contents = json_decode($file->read());
      //  echo var_dump($contents);
        $this->set('contents',$contents);
        $this->set('_serialize', ['contents']);
        
	}

	public function index()
	{
		$this->viewBuilder()->layout('toppage');
		//新着不動産相談
		$questions = $this->Questions;
		$query = $questions
            ->find()
            ->order(['created' => 'DESC'])
            ->limit(5);
		$this->set('results',$query);
		$this->set('_serialize', ['results']);
		
		
        //ようこそ表示用名前取得
        // if(isset($current_user)){
        //     $temp1 = $current_user['email'];
        //     $end1 = stripos($temp1,'@');
        //     $name1 = substr($temp1,0,$end1);
        //     $this->set('name1',$name1);
        // };
        
        //専門家会員数取得用変数
        // $temp = TableRegistry::get('experts');
        // $query = $temp->find();
        // $total = [];
        // foreach ($query as $row):
        //     $total[] = $row->id;
        // endforeach;
        // $this->set('num_ex', count($total));
        // unset($temp);
        
        //キーワード検索用
        $keywords = $this->Questions->newEntity();
        $this->set('keywords',$keywords);
        $this->set('_serialize', ['keywords']);
        if ($this->request->is('post')) {
        	return $this->redirect(['action' => 'search']);
        };
        
	}
	public function inventoryL($category_l=null)
	{
		$this->viewBuilder()->layout('inventory_l');
		
		// $category_l="l1";
		
		//引数をset
		$this->set(compact('category_l'));
		//現在の引数のカテゴリをset
		$current_category = $this->get_category($category_l);
		$this->set(compact('current_category'));
		
		//カテゴリ該当記事を新着順に表示
		$large_category_id = substr($category_l,1);
		
		// $questions = TableRegistry::get('questions');
		$questions = $this->Questions;
		$query = $questions
            ->find()
            ->where(['large_category_id' => $large_category_id])
            ->order(['created' => 'DESC']);
            // ->all();
        // debug($query);
        $this->set('results', $this->paginate($query));
		$this->set('_serialize', ['results']);
		
		//別のとり方。paginate出来なかった。
		// $connection = ConnectionManager::get('c9fdo_db');
		// $results = $connection->execute('SELECT * FROM questions WHERE large_category_id = :large_category_id ORDER BY created DESC ', ['large_category_id' => $large_category_id])->fetchAll('assoc');
		// $this->set('results',$results);
		
	}
	
	public function inventoryM($category_l=null,$category_m=null)
	{
		$this->viewBuilder()->layout('inventory_m');
		//引数をset
		$this->set(compact('category_l','category_m'));
		//現在の引数のカテゴリをset
		$current_category = $this->get_category($category_l,$category_m);
		$this->set(compact('current_category'));
		$this->set('_serialize', ['current_category']);
		
		//カテゴリ該当記事を新着順に表示
		$middle_category_id = substr($category_m,1);
		
		// $questions = TableRegistry::get('questions');
		$questions = $this->Questions;
		$query = $questions
            ->find()
            ->where(['middle_category_id' => $middle_category_id])
            ->order(['created' => 'DESC']);
            // ->all();
        $this->set('results', $this->paginate($query));
		$this->set('_serialize', ['results']);
		
		// $connection = ConnectionManager::get('c9fdo_db');
		// $results = $connection->execute('SELECT * FROM questions WHERE middle_category_id = :middle_category_id ORDER BY created DESC ', ['middle_category_id' => $middle_category_id])->fetchAll('assoc');
		// $this->set(compact('results'));
		// $this->set('_serialize', ['results']);
	}
	public function inventoryS($category_l=null,$category_m=null,$category_s=null)
	{
		$this->viewBuilder()->layout('inventory_s');
		//引数をset
		$this->set(compact('category_l','category_m','category_s'));
		//現在の引数のカテゴリをset
		$current_category = $this->get_category($category_l,$category_m,$category_s);
		$this->set(compact('current_category'));
		$this->set('_serialize', ['current_category']);
		
		//カテゴリ該当記事を新着順に表示
		$small_category_id = substr($category_s,1);
		
		$questions = $this->Questions;
		$query = $questions
            ->find()
            ->where(['small_category_id' => $small_category_id])
            ->order(['created' => 'DESC']);
            // ->all();
        $this->set('results', $this->paginate($query));
		$this->set('_serialize', ['results']);
		
	}
	public function search()
	{
	    $this->viewBuilder()->layout('general');
	    
	    // 仮
	    $category_l = "l1";
	    $category_m = "m12";
	    $category_s = "s123";
	    
	    $this->set(compact('category_l','category_m','category_s'));
		//現在の引数のカテゴリをset
		$current_category = $this->get_category($category_l,$category_m,$category_s);
		$this->set(compact('current_category'));
	    
	    //記事を新着順に表示
		$questions = $this->Questions;
		$query = $questions
            ->find()
            ->order(['created' => 'DESC']);
            // ->all();
        $this->set('results', $this->paginate($query));
		$this->set('_serialize', ['results']);
	    
	    
        //キーワード検索用
        $keywords = $this->Questions->newEntity();
        $this->set('keywords',$keywords);
        $this->set('_serialize', ['keywords']);
        if ($this->request->is('post')) {
        	return $this->redirect(['action' => 'search']);
        };
	}
	public function view($question_id=null)
	{
	    $this->viewBuilder()->layout('general');
	    
	    //指定質問の表示
		$questions = $this->Questions;
		$result = $questions
			->find()
			->where(['Questions.id'=>$question_id])
			// ->first();
			// ->all();
			->toArray();
		$result = $result[0];
		// debug($result);
		$this->set(compact('result'));
		
		//回答と返信の表示
		$answers = $this->Answers;
		$result_answers = $answers
			->find()
			->where(['Answers.question_id'=>$question_id])
			// ->all();
			->toArray();
		$this->set(compact('result_answers'));
		// debug($result_answers);
		
	    $category_l = 'l' . $result->large_category_id;
	    $category_m = 'm' . $result->middle_category_id;
	    $category_s = 's' . $result->small_category_id;
	    $current_category = $this->get_category($category_l,$category_m,$category_s);
		$this->set(compact('current_category','category_l','category_m','category_s'));
		
		// 関連相談　指定
		$relation_questions = $this->Questions
			->find()
			->where(['Questions.id'=> 121])
			->orWhere(['Questions.id'=> 122])
			->toArray();
		$this->set(compact('relation_questions'));
		
		// ユーザーズリスト
		$this->set('users_list',$this->Users->find()->toArray());
		
        //キーワード検索用
        $keywords = $this->Questions->newEntity();
        $this->set('keywords',$keywords);
        $this->set('_serialize', ['keywords']);
        if ($this->request->is('post')) {
        	return $this->redirect(['action' => 'search']);
        };
		
	}
	public function add()
	{
	    $this->viewBuilder()->layout('form');

	    $question = $this->Questions->newEntity();
        if ($this->request->is('post') && isset($_POST['submit'])) {
            $question = $this->Questions->patchEntity($question, $this->request->data);
            if ($this->Questions->save($question)) {
                // $this->Flash->success(__('The article has been saved.'));
                // return $this->redirect(['controller' => 'Users' , 'action' => 'mypage']);
            }else{
            $this->Flash->error(__('投稿に失敗しました。文字数を確認して再度投稿してください。'));
            $error_flag = 1;
            $this->set(compact('error_flag'));
            }
        }
        // $users = $this->questions->Users->find('list', ['limit' => 200]);
        // $midCategories = $this->questions->MidCategories->find('list', ['limit' => 200]);
        // $this->set(compact('question', 'users', 'midCategories'));
        $this->set(compact('question'));
        $this->set('_serialize', ['question']);
        
        //カテゴリ選択リストの為にjsonをctpに渡す。
        //$file = new file('../src/Model/categories.json');
        $file = new File('src/Model/categories.json');

        $contents = json_decode($file->read());
        $this->set(compact('contents'));
	}
	public function underConstruction()
	{
		$this->viewBuilder()->layout('under_construction');
		$pre_url = $this->referer();
		$this->set(compact('pre_url'));
	}
}