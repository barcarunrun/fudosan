<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\View\Helper\HtmlHelper;
use Cake\View\Helper\FormHelper;

/**
 * Articles Controller
 *
 * @property \App\Model\Table\ArticlesTable $Articles
 */
class ArticlesController extends AppController
{
    public $paginate = [
        'limit' => 3,
        'order' => [
            'Articles.created' => 'desc'
        ]
    ];
    public $helpers = [
    'Paginator' => ['templates' => 
        'paginator-templates']
    ];
    public function initialize()
	{
		parent::initialize();
		$this->viewBuilder()->layout('pattern03');
		$this->loadComponent('Paginator');
		
		//ログイン状態の確認用変数取得
        $current_user = $this->Auth->user();
        $this->set('current_user', $current_user);
        
        //large_categoriesの分類抽出
        $large_categories = TableRegistry::get('large_categories');
        $query = $large_categories->find();
        $large_categorie_list = [];
        foreach ($query as $row):
            $large_categorie_list[] = $row->name;
        endforeach;
        $this->set('large_categorie_list',$large_categorie_list);
        unset($query);
        
        //mid_categoriesの分類抽出
        //mid_categories_id から large_cateogries_idを取得
        $mid_categories = TableRegistry::get('mid_categories');
        $query = $mid_categories->find();
        $mid_categorie_list = [];
        $mid_large_list = [];
        foreach ($query as $row):
            $mid_categorie_list[] = $row->name;
            $mid_large_list[] = $row->large_category_id;
        endforeach;
        $this->set('mid_categorie_list',$mid_categorie_list);
        $this->set('mid_large_list',$mid_large_list);
        unset($query);
        
        $temp = TableRegistry::get('prefectures');
        $this->set('prefectures',$temp->find('list',array('fields'=>array('id','name'))));
        unset($temp);
        
	}

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index($num01 = null)
    {
        //タイトル表示用（一括定義できないか？）
        if(is_null($num01)){
            $num01 = 10;
        };
        $large_num = $num01 -1;
        $this->set('large_num',$large_num);
        
        $this->set('articles', $this->paginate());
        
        // aside02カテゴリリンク用
        $mid_categories = TableRegistry::get('mid_categories');
        if($num01==10){
            $num03 = 1;
        }else{
            $num03 = $num01;
        };
        $thearticles = $mid_categories
        ->find('all')
        ->where(['large_category_id' => $num03]); 
        $this->set('thearticles', $thearticles);
        
        $this->set(compact("num01", "num03"));
    }
    public function categorieindex($num01 = null,$num02 =null)
    {
        // $this->loadComponent('Paginator');
        $articles = TableRegistry::get('questions');
        $mid_categories = TableRegistry::get('mid_categories');//initializeで呼んでるのに
        
        //タイトル表示用
        if(is_null($num01)){
            $num01 = 10;
        };
        $large_num = $num01 -1;
        $this->set('large_num',$large_num);
        
        // 記事表示用
        $choose_articles = $mid_categories
        ->find('all')
        // ->select('*')
        ->select('id','name')
        // ->select('id','large_category_id','name')
        ->where(['large_category_id' => $num01]); 
        if(is_null($num02)){
            $query = $articles
            ->find('all')
            ->where(['mid_category_id IN' => $choose_articles]);    
        }else{
            $query = $articles
            ->find('all')
            ->where(['mid_category_id' => $num02]);
            // ->all();
        };
        $this->set('articles', $this->paginate($query));
        // $this->set('choose_articles', $choose_articles);
        // $articles = $this->paginate($query);
        // $this->set(compact('articles'));
        $this->set('_serialize', ['articles']);
        // $this->set('kkk',count($choose_articles));
        $this->set('num02',$num02);
        $this->set('mid_categories',$mid_categories);
        
        // aside02カテゴリリンク用
        if($num01==10){
            $num03 = 1;
        }else{
            $num03 = $num01;
        };
        $thearticles = $mid_categories
        ->find('all')
        ->where(['large_category_id' => $num03]); 
        $this->set('thearticles', $thearticles);
        
        $this->set(compact("num01", "num03"));
        
    }
    /**
     * View method
     *
     * @param string|null $id Article id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $article = $this->Articles->get($id, [
            'contain' => ['Users', 'MidCategories']
        ]);

        $this->set('article', $article);
        $this->set('_serialize', ['article']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $article = $this->Articles->newEntity();
        if ($this->request->is('post')) {
            $article = $this->Articles->patchEntity($article, $this->request->data);
            if ($this->Articles->save($article)) {
                $this->Flash->success(__('The article has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The article could not be saved. Please, try again.'));
        }
        $users = $this->Articles->Users->find('list', ['limit' => 200]);
        $midCategories = $this->Articles->MidCategories->find('list', ['limit' => 200]);
        $this->set(compact('article', 'users', 'midCategories'));
        $this->set('_serialize', ['article']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Article id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $article = $this->Articles->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $article = $this->Articles->patchEntity($article, $this->request->data);
            if ($this->Articles->save($article)) {
                $this->Flash->success(__('The article has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The article could not be saved. Please, try again.'));
        }
        $users = $this->Articles->Users->find('list', ['limit' => 200]);
        $midCategories = $this->Articles->MidCategories->find('list', ['limit' => 200]);
        $this->set(compact('article', 'users', 'midCategories'));
        $this->set('_serialize', ['article']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Article id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $article = $this->Articles->get($id);
        if ($this->Articles->delete($article)) {
            $this->Flash->success(__('The article has been deleted.'));
        } else {
            $this->Flash->error(__('The article could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    public function searchLarge()
    {
        $this->viewBuilder()->layout('pattern02');
    }
    public function searchMedium()
    {
        $this->viewBuilder()->layout('pattern02');
    }
    public function searchResult()
    {
        $this->viewBuilder()->layout('pattern02');
    }
}
